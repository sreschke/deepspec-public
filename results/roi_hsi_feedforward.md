FIXME: Finish writing this


Using this training set

![Processed_HSI_spectra_samples](https://bitbucket.org/sreschke/deepspec/raw/d85894eac75e6baae21ee2ee0d8429e0db4a600c/deepspec/images/ROI_train_image.png)

And this validation set

![Processed_HSI_spectra_samples](https://bitbucket.org/sreschke/deepspec/raw/d85894eac75e6baae21ee2ee0d8429e0db4a600c/deepspec/images/ROI_val_image.png)

The network makes the following predictions

![Processed_HSI_spectra_samples](https://bitbucket.org/sreschke/deepspec/raw/d85894eac75e6baae21ee2ee0d8429e0db4a600c/deepspec/images/t%3D0.200000_predictions.gif)
![Processed_HSI_spectra_samples](https://bitbucket.org/sreschke/deepspec/raw/d85894eac75e6baae21ee2ee0d8429e0db4a600c/deepspec/images/t%3D0.999990_predictions.gif)
