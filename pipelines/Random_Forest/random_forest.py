# -*- coding: utf-8 -*-
from deepspec.cuprite.data_processing.hsi import get_cuprite_hsi_cube,\
hsi_preprocessing, get_kruse_labels, get_ROI_labels
from deepspec.cuprite.utility.misc import section
from sklearn.ensemble import RandomForestClassifier
from sklearn.metrics import classification_report, confusion_matrix

if __name__ == "__main__":    
    ###########################################################################
    #Load data
    ###########################################################################        
    section("Load Data")
    
    #Load Ground Truth
    print("Loading Kruse Ground Truth...")
    ground_truth = get_kruse_labels()
    ground_truth.show()
    
    #Load Roi 
    print("Loading Roi...")
    roi = get_ROI_labels()
    roi.show()
    
    #Load spectra
    print("Loading spectra...")
    unp_HSI_spectra = get_cuprite_hsi_cube()
    
    ###########################################################################
    #Data Preprocessing
    ###########################################################################
    section("Data Preprocessing")
    pr_spectral_cube, _ = hsi_preprocessing(spectral_cube=unp_HSI_spectra)
    del unp_HSI_spectra #recover memory
    
    ###########################################################################
    #Train set
    ###########################################################################
    section("Train set")
    print("Creating training Set...")
    train = roi.union(ground_truth.random_sample(per=0.01), name="Train")    
    X_train, y_train, train_settings = train.flatten(pr_spectral_cube)
    train.show()
    train.bar_chart(count=True, live_only=True)
    
    ###########################################################################
    #Test set
    ###########################################################################
    print("Creating test set...")
    test = ground_truth.copy("Test")
    X_test, y_test, test_settings = test.flatten(pr_spectral_cube)
    test.show()
    test.bar_chart(count=True, live_only=True)
    
    ###########################################################################
    #Random Forest
    ###########################################################################
    section("Random Forest")
    print("fitting on training data...")
    classifier = RandomForestClassifier(n_estimators=100,
                                        criterion="entropy",
                                        max_depth=20)
    classifier.fit(X_train, y_train)
    
    ###########################################################################
    #Model evaluation
    ###########################################################################
    section("Model evaluation")
    print("Getting test predictions...")
    y_pred = classifier.predict(X_test)
    
    print("Confusion matrix")
    print(confusion_matrix(y_test, y_pred))
    
    print("\nClassification report...")
    print(classification_report(y_test, y_pred, target_names=test_settings.CATEGORIES[:-2]))