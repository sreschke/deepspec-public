from deepspec.cuprite.data_processing.hsi import get_cuprite_hsi_cube,\
hsi_preprocessing, get_kruse_labels, get_ROI_labels
from deepspec.cuprite.utility.misc import section
import os
from sklearn.neighbors import KNeighborsClassifier
from sklearn.metrics import classification_report, confusion_matrix

if __name__ == "__main__":
    model_name = "ROI_demo"
    save_dir = os.getcwd() + os.sep + "ROI_demo_results"    
    ###########################################################################
    #Load data
    ###########################################################################        
    section("Load Data")
    
    #Load Ground Truth
    print("Loading Kruse Ground Truth...")
    ground_truth = get_kruse_labels()
    
    #Load Roi 
    print("Loading Roi...")
    roi = get_ROI_labels()
    
    #Load spectra
    unp_HSI_spectra = get_cuprite_hsi_cube()
    
    ###########################################################################
    #Data Preprocessing
    ###########################################################################
    section("Data Preprocessing")
    pr_spectral_cube, pr_indices = hsi_preprocessing(spectral_cube=unp_HSI_spectra)
    del unp_HSI_spectra #recover memory
    
    ###########################################################################
    #Train set
    ###########################################################################
    section("Train set")
    train = roi.union(ground_truth.random_sample(per=0.05), name="Train")    
    X_train, y_train, train_settings = train.flatten(pr_spectral_cube)
    train.show()
    
    ###########################################################################
    #Test set
    ###########################################################################
    X_test, y_test, test_settings = ground_truth.flatten(pr_spectral_cube)
    ground_truth.show()
    
    ###########################################################################
    #K-nearest neighbors
    ###########################################################################
    section("K-nearest neighbors")
    print("fitting on training data...")
    num = 5
    classifier = KNeighborsClassifier(n_neighbors=num,  p=2) #Using Euclidean distance
    classifier.fit(X_train, y_train)
    
    ###########################################################################
    #Model evaluation
    ###########################################################################
    section("Model evaluation")
    print("Test predictions...")
    y_pred = classifier.predict(X_test) #Will take a while
    
    print("Confusion matrix")
    print(confusion_matrix(y_test, y_pred))
    
    print("\nClassification report...")
    print(classification_report(y_test, y_pred, target_names=test_settings.CATEGORIES[:-2]))
    
    
    
    
    
    