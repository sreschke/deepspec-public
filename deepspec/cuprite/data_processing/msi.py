# -*- coding: utf-8 -*-
from deepspec.cuprite.utility import utils
import spectral.io.envi as envi
import numpy as np
import os

def get_cuprite_msi_cube(hdr_path=None, img_path=None):
    """
    Retrieves the Cuprite MSI spectral cube from the Envi files
    
    Paramters:
        img_path (str) optional: path to the Envi .img file
        hdr_path (str) optional: path to the Envi .hdr file
        
    Returns:
        ((m, n, o) ndarray): the spectral cube in numpy format    
    """
    
    if hdr_path is not None:
        assert img_path is not None, "img_path must be specified if hdr_path is specified"
    else:
        hdr_path = utils.DATA_DIR + "WV3" + os.sep + "WV3-7pt5m-Kruse.hdr"
        img_path = utils.DATA_DIR + "WV3" + os.sep + "WV3-7pt5m-Kruse.dat"
    
    img = envi.open(hdr_path, img_path)
    spectra = np.array(img.asarray())
    return spectra

def msi_preprocessing(spectral_cube):
    """
    Data prepocessing of msi spectra. Includes weight adjustment
    
    Parameters:
        spectral_cube ((m, n, o) ndarray): the HSI data cube
        
    Returns:
        ((m, n, o) ndarray): the processed spectral data
    """
    #Check input
    assert type(spectral_cube) == np.ndarray, "spectral_cube must be of type ndarray"
    assert spectral_cube.ndim == 3, "spectral_cube must be three dimensional"
    
    #Clean and Process Data
    spectral_cube = spectral_cube/utils.WEIGHT
    
    return spectral_cube