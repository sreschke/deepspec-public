# -*- coding: utf-8 -*-

from deepspec.cuprite.utility import utils
from deepspec.cuprite.utility.pipe_settings import pipe_settings
from deepspec.cuprite.utility.pipe_labels import pipe_labels
import spectral.io.envi as envi
import numpy as np
import os
import csv   

def hsi_preprocessing(spectral_cube, verbose=True):
    """
    Data prepocessing of hsi spectra. Includes weight adjustmnet, removing
    absorption lines and removing negative values.
    
    Parameters:
        spectral_cube ((m, n, o) ndarray): the HSI data cube
        verbose (bool): whether to print function progress. Default is True
        
    Returns:
        ((m, n, o) ndarray): the processed spectral data
        (p, ) ndarray: FIXME
    """
    #Check input
    assert type(spectral_cube) == np.ndarray, "spectral_cube must be of type ndarray"
    assert spectral_cube.ndim == 3, "spectral_cube must be three dimensional"
    
    #Clean and Process Data
    if verbose:
        print("Processing Data...")
        print("\tWeight adjustment...")
    spectral_cube = spectral_cube/utils.WEIGHT
    
    if verbose:
        print("\tLocating non-dead spectra...")
    live_mask = ~get_dead_mask()
    count = np.count_nonzero(live_mask)
    
    if verbose:
        print("\tFound {} non-dead spectra".format(count))
        print("\tRemoving absorption lines...")
        print("\tRemoving negative values...")
        
    #Get first non-dead spectrum
    locs = np.argwhere(live_mask)[0]
    row, col = locs[0], locs[1]
    temp = spectral_cube[row, col, :]
    
    #Remove absorption lines
    abs_mask = temp != 0.0
    indices = np.arange(len(temp))[abs_mask]
    spectral_cube = spectral_cube[:, :, indices]
    
    #Remove negative values from non-deads
    live_spectra = spectral_cube[live_mask]
    neg_mask = live_spectra < 0
    live_spectra[neg_mask] = 0
    spectral_cube[live_mask] = live_spectra
    
    assert np.all(spectral_cube[live_mask] >= 0.0), "Removing negative values has failed"
    
    return spectral_cube, indices

def get_cuprite_hsi_cube(hdr_path=None, img_path=None):
    """
    Retrieves the Cuprite HSI spectral cube from the Envi files.
    
    Parameters:
        settings (settings): a settings class instantiation
        img_path (str) optional: path to the Envi .img file
        hdr_path (str) optional: path to the Envi .hdr file
        
    Returns:
        ((m, n, o) ndarray): the spectral cube in numpy format    
    """
    if hdr_path is not None:
        assert img_path is not None, "img_path must be specified if hdr_path is specified"
    else:
        hdr_path = utils.DATA_DIR + "AVIRIS" + os.sep + "AVIRIS-7pt5-Kruse.hdr"
        img_path = utils.DATA_DIR + "AVIRIS" + os.sep + "AVIRIS-7pt5-Kruse.bil"
    
    img = envi.open(hdr_path, img_path)
    spectra = np.array(img.asarray())
    return spectra

def get_dead_mask():
    """Retrieves the locations of the dead pixels
    
    The array in dead_mask.npz was generated by applying is_bad_spectrum() to
    each spectrum in the HSI data cube.
    
    Returns:
        ((1382, 1207) ndarray): a numpy boolean array
    """
    file_path = utils.DATA_DIR + "AVIRIS" + os.sep + "dead_mask.npz"
    npz_file = np.load(file_path)
    deads = npz_file["deads"]
    return deads

def get_kruse_labels(hdr_path=None, img_path=None, hsi=True):
    """
    Retrieves the Kruse 'ground truth' labels from Envi files and returns a
    pipe_labels instance
    
    Parameters:
        img_path (str) optional: path to the Envi .img file
        hdr_path (str) optional: path to the Envi .hdr file
        hsi (bool) optional: whether the labels will be used for hsi spectra.
            If True, the function labels the dead pixels
        msi_adjust (bool): Whether to adjust the labels for the msi data.
        
    Returns:
        (pipe_labels): a pipe_labels class instance    
    """
    categories = ['Alunite', 'Buddingtonite', 'Kaolinite', 'Calcite', 'Silica',
                  'Muscovite', 'Unclassified', 'Dead']
    colors = ['red', 'cyan', 'yellow', 'blue', 'magenta', 'lime', 'gray', 'black']
    if not hsi:
        categories = categories[:-1]
        colors = colors[:-1]
    settings = pipe_settings(categories, colors)
    
    if hdr_path is not None:
        assert img_path is not None, "img_path must be specified if hdr_path is specified"
    else:
        hdr_path = utils.DATA_DIR + "AVIRIS" + os.sep + "Classification_Image.hdr"
        img_path = utils.DATA_DIR + "AVIRIS" + os.sep + "Classification_Image.img"
    
    NUM_ROWS, NUM_COLS = utils.NUM_ROWS, utils.NUM_COLS
    labels_img = envi.open(hdr_path, img_path)
    labels = np.array(labels_img.asarray().reshape(NUM_ROWS, NUM_COLS))
    
    #Match labels with settings
    labels = _match_labels(labels, settings)
    
    if hsi:
        #Add "dead" labels
        deads = get_dead_mask()
        labels[deads] = settings.INT_MAP["Dead"]
        
    if not hsi:
        #Realign Spectra and labels
        x_shift = 2 #(in pixels)
        labels[:, :-x_shift] = labels[:, x_shift:]
        labels[:, -x_shift:] = settings.INT_MAP[utils.UNCLASS_NAME]
    return pipe_labels(categories=categories,
                       colors=colors,
                       labels=labels,
                       name="Kruse Ground Truth")

def get_ROI_labels(data_path=None, dead=True):
    """
    Retrieves the Regions of Interest (ROIs) from Kruse Analysis and returns
    them as a pipe_labels instance
    
    Paramters:
        data_path (str) optional: where to load the appropriate csv file 
            (from Chris). If None, the function looks for the file in the
            data/AVIRIS directory
        dead (bool): whether to label the 'dead' spectra

    Returns:
        (pipe_labels): a pipe_labels instantiation        
    """
    if data_path is None:
        data_path = utils.DATA_DIR + "AVIRIS" + os.sep + "AVIRIS-ROI-spectra.csv"
        
    categories = ['Alunite', 'Buddingtonite', 'Kaolinite', 'Calcite', 'Silica',
                  'Muscovite', utils.UNCLASS_NAME]
    colors = ['red', 'cyan', 'yellow', 'blue', 'magenta', 'lime', 'gray']
    if dead:
        categories.append(utils.DEAD_NAME)
        colors.append("black")
    settings = pipe_settings(categories=categories, colors=colors)
    
    labels = np.ones((utils.NUM_ROWS, utils.NUM_COLS))*settings.INT_MAP[utils.UNCLASS_NAME]
    if dead:
        dead_mask = get_dead_mask()
        labels[dead_mask] = settings.INT_MAP[utils.DEAD_NAME]
    with open(data_path) as csvfile:
        readCSV = csv.reader(csvfile, delimiter=",")
        for row in readCSV:
            material, col, row = row
            row = int(row)
            col = int(col)
            if material in settings.CATEGORIES:
                labels[row, col] = settings.INT_MAP[material]
            else:
                labels[row, col] = settings.INT_MAP[utils.UNCLASS_NAME]
                
    return pipe_labels(categories=categories,
                       colors=colors,
                       labels=labels,
                       name="Kruse ROI")
    
def is_bad_spectrum(spectrum):
    """
    Decides whether a given spectrum is 'bad'. Bad spectra are spectra with all
    negative values. Includes 'dead' spectra.
    
    Parameters:
        spectrum ((m, ) ndarray): a spectrum
        
    Returns:
        (bool): true if the spectrum is bad, false otherwise        
    """    
    if np.alltrue(spectrum < 0):
        return True
    else:
        return False
    
def _match_labels(labels, settings):
    """
    Helper function
    
    Matches labels with settings. Assumes the labels are aligned with Kruse's
    implicit settings as specified in utils.KR_INT_MAP
    
    Parameters:
        labels ((m, n) ndarray)): the integer labels to line up
        settings (pipe_settings): a pipe_settings class instantiation
    
    Returns:
        ((m, n) ndarray): the aligned labels
    """
    #Match labels with settings file
    al_labels = labels.copy()
    for cat in utils.KR_CATEGORIES:
        search_int = utils.KR_INT_MAP[cat]
        mask = labels==search_int
        if cat in settings.CATEGORIES:
            tar_int = settings.INT_MAP[cat]
        else:
            tar_int = settings.INT_MAP[utils.UNCLASS_NAME]
        al_labels[mask] = tar_int
    return al_labels
    
def remove_negative(array):
    """
    Identifies regions in an array with negative values and replaces them with
    zeros. Changes the array in place (the original is modified and returned).
    
    Paramters:
        array ((n, ) np.array): a numpy array
    """
    
    #Identify where array is negative
    mask = array < 0
    
    #Set negative regions to zero
    array[mask] = 0
    
    return array