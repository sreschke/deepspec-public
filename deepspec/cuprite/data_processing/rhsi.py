# -*- coding: utf-8 -*-
import numpy as np
import scipy
import os
from deepspec.cuprite.utility import utils

###############################################################################
#FIXME: this file is outdated
###############################################################################

def get_rhsi_data(data_path=None, dead=True, verbose=True):
    """
    Retrieves the Cuprite rHSI spectral cube from a matplotlib file
    
    Paramters:
        data_path (str) optional: the path to the resampled data. If not
            specified, the function looks for the data in the AVIRIS data folder.
        dead (bool): whether to label the dead pixels
        verbose (bool): whether to print function progress
        
    Returns:
        spectra ((m, n, o) ndarray): the spectral cube in numpy format
        labels ((m, n) ndarray): the spectral labels (in integer, zero-indexing
               format)
    """
    
    #Load Matlab file
    if verbose:
        print("Loading matlab file...")
    if data_path is None:
        data_name = "16Bands.mat"
        data_path = utils.DATA_DIR + "AVIRIS" + os.sep + data_name
    mat = scipy.io.loadmat(data_path)
    
    #Retrieve Data
    spectra = mat["resampHSI"]
    labels = mat["label"]
    
    if verbose:
        print("Transforming Data...")
    #FLIP Spectra (THEY ARE FLIPPED IN THE MATLAB FILE. Ask Chris about it)
    spectra = np.flip(spectra, axis=0)
    
    #Transpose
    spectra = np.transpose(spectra, axes=[1, 0, 2]) #(1207, 1382, 16) --> (1382, 1207, 16)
    labels = np.transpose(labels, axes=[1, 0]) #(1207, 1387) --> (1387, 1207)
    
    spectra = spectra/utils.WEIGHT
    #Make sure spectra and labels are lined up
    sshape = spectra.shape
    lshape = labels.shape
    assert sshape[0] == lshape[0] and sshape[1] == lshape[1], "Spectra and labels aren't lined up"
    
    #Reformat labels to match with settings
    labels = match_labels(labels)    
    
    if dead:
        deads = np.apply_along_axis(is_bad_spectrum, axis=2, arr=spectra)
        labels[deads] = settings.INT_MAP["Dead"]
        
    return spectra, labels