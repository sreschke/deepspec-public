# -*- coding: utf-8 -*-
import numpy as np
from tensorflow.keras.models import load_model
import pickle
from deepspec.cuprite.utility import utils
from deepspec.cuprite.data_processing.hsi import get_dead_mask

def softmax_cube(settings, spectral_cube=None, model_path=None,
                 pca_path=None, verbose=True):
    """
    Calcualte a softmax prediction for each pixel in a spectral cube. An array
    of negative ones are predicted for "Dead" spectra.    
        
    Parameters:
        settings (settings): a settings class instantiation
        spectral_cube ((m, n, o) ndarray): the HSI processed data cube
        model_path (str): path to the trained neural network model (the .h5 file)
        pca_path (str) optional: path to the saved PCA transformation 
            (the .sav file). If None, PCA is not performed
        thresholds (list(floats)): what threshold(s) to use. The thresholds
            must be between 0 and 1 exclusive.
        softmax_cube ((m, n, o) ndarray) optional: The class distributions for
            each pixel. 
        verbose (bool): whether to print function progress
    
    Returns:
        softmax_cube ((m, n, o) ndarray): The class distributions for each
            pixel.
    """
    #Check input
    assert type(spectral_cube) == np.ndarray, "spectral_cube must be of type ndarray"
    assert type(model_path) == str, "model_path must be of type str"
    if pca_path is not None:
        assert type(pca_path) == str, "pca_path must be of type str"
    assert spectral_cube.ndim == 3, "spectral_cube must be 3 dimensional"
    
    #Useful variables
    num_rows = utils.NUM_ROWS
    num_cols = utils.NUM_COLS
    
    if verbose:
        print("Loading Model...")
    #Load Pre-Trained Neural Network Model
    model = load_model(model_path)
    
    if verbose:
        print("Locating Non Deads...")
       
    #Retrive spectra to classify
    if utils.DEAD_NAME in settings.INT_MAP.keys():
        mask = ~get_dead_mask()
        to_classify = spectral_cube[mask]
    else:
        mask = np.ones((num_rows, num_cols))
        to_classify = spectral_cube.reshape(utils.NUM_ROWS*utils.NUM_COLS, spectral_cube.shape[-1])
    
    #Perform pca if needed
    if pca_path is not None:
        if verbose:
            print("PCA...")
        #Load PCA model
        pca = pickle.load(open(pca_path, "rb"))
        to_classify = pca.transform(to_classify)
    
    #Predict softmax outputs
    if verbose:
        print("Predicting Distributions...")
    softmax_outs_flat = model.predict(to_classify)
    
    #Reshape into cube
    num_chans = softmax_outs_flat.shape[-1]
    softmax_cube = -np.ones((num_rows, num_cols, num_chans))
    softmax_cube[mask] = softmax_outs_flat
    
    return softmax_cube

def get_predictions(settings, softmax_cube, threshold):
    """"
    Retrieves predictions given a softmax_cube and threshold values. 
    
    Parameters:
        settings (pipe_settings): a pipe_settings instance
        softmaxcube ((m, n, o) ndarray): a softmax_cube as given by the function
            softmax_cube()
        threshold (float): the threshold value to use
    Returns:
        outs ((m, n, o) ndarray): sorted integer class predictions. The operation
            outs[:, :, 0] retrieves the primary predictions; outs[:, :, 1]
            retrieves the secondary predictions, and so forth. 
    """
    
    #Initialize outputs
    outs = np.ones(softmax_cube.shape)*settings.INT_MAP[utils.UNCLASS_NAME]
    
    #Find rows/cols where softmax output is greater than threshold
    mask = np.any(softmax_cube > threshold, axis=2)
    
    #Get sorted predictions that are above threshold value
    temp = softmax_cube[mask]
    args = np.argsort(-temp, axis=1)
    temp = np.sort(temp, axis=1)
    mask2 = np.flip(temp < threshold, axis=1)
    #Set values that are below threshold to be unclassified
    args[mask2] = settings.INT_MAP[utils.UNCLASS_NAME]
    
    #Update output
    outs[mask] = args 
    
    return outs