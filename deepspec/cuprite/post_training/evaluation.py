# -*- coding: utf-8 -*-
import numpy as np
import matplotlib.pyplot as plt
#from deepspec.cuprite.data_processing.hsi import get_kruse_ground_truth
from scipy import interp
from sklearn.metrics import roc_curve, auc, precision_recall_curve, average_precision_score
from sklearn.metrics import classification_report
from contextlib import redirect_stdout

###############################################################################
#FIXME: this file needs to be updated
###############################################################################
def model_evaluation(settings, true_labels, softmax_output, thresholds, argmax, save_path):
    """Creates a model evaluation report
    
    Parameters:
        settings (settings): a settings class instantiation
        true_labels ((m, ) ndarray): the correct lables in normal format
            E.g. [0, 2, 1] (not one-hot encoded)
        softmax_output ((m, n), ndarray): the softmax output from a neural
            network
        categories ((n, ) list or ndarray): the material names
        thresholds (list or ndarray): the thresholds to use on the softmax output
        argmax (bool): whether to use argmax thresholding,
        save_path (str): where to save the resulting file
        
    Generates:
        A classification report saved to save_path
    """
    #Check input
    assert type(true_labels) == np.ndarray, "true_lables must be of type ndarray"
    assert type(softmax_output) == np.ndarray, "softmax_output must be of type ndarray"
    assert type(thresholds) in [list, np.ndarray], "thresholds must be of type list or ndarray"
    if len(thresholds) > 0:
        for t in thresholds:
            assert 0 < t < 1, "all thresholds must be between 0 and 1"
    assert type(argmax) == bool, "argmax must be of type bool"
    assert type(save_path) == str, "save_path must be of type str"    
    assert argmax or len(thresholds) > 0,  "argmax must be true or thresholds must be non-empty"
    
    #FIXME: this next line might not work
    categories = settings.get_categories(labels=true_labels)
    with open(save_path, "w") as f:
        with redirect_stdout(f):
            #argmax threshold
            if argmax:
                test_predictions = np.argmax(softmax_output, axis=1)
                report = classification_report(true_labels, test_predictions, target_names=categories)
                print("argmax threshold")
                print(report)
                print()
            if len(thresholds) > 0:
                for t in thresholds:
                    f = lambda x: np.argmax(x) if np.max(x) > t else 0
                    test_predictions = np.apply_along_axis(f, axis=1, arr=softmax_output)
                    report = classification_report(true_labels, test_predictions, target_names=categories)
                    print("threshold = {:.3f}".format(t))
                    print(report)
                    print() 

def get_pr_data(one_hot_labels, softmax_output):
    """FIXME
    
    Parameters:
        one_hot_labels ((m, n) ndarray): the true labels (in one-hot-encoded or 
            binary format). E.g. the labels [0, 1, 2] would become
            [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        
        softmax_output ((m, n) ndarray): the softmax_output from a neural
            network classifier.     
    """
    #Make sure one_hot_labels are type int
    one_hot_labels = one_hot_labels.astype(int)
    
    n_classes = len(one_hot_labels[0])
    precs = dict()
    recs = dict()
    aucs = dict()
    #Compute PR curve, average precision, and PR area for each class
    for i in range(n_classes):
        precs[i], recs[i], thresholds = precision_recall_curve(one_hot_labels[:, i], softmax_output[:, i])
        aucs[i] = average_precision_score(one_hot_labels[:, i], softmax_output[:, i])
        
    #Calculate micro-average versions
    precs["micro"], recs["micro"], thresholds = precision_recall_curve(one_hot_labels.ravel(), softmax_output.ravel())
    aucs["micro"] = average_precision_score(one_hot_labels, softmax_output, average="micro")
    
    return precs, recs, aucs

def get_roc_auc_data(one_hot_labels, softmax_output):
    """
    Parameters:
        one_hot_labels ((m, n) ndarray): the true labels (in one-hot-encoded or 
            binary format). E.g. the labels [0, 1, 2] would become
            [[1, 0, 0], [0, 1, 0], [0, 0, 1]]
        
        softmax_output ((m, n) ndarray): the softmax_output from a neural
            network classifier.        
        
    Returns:
        fpr (dict): the false positive rates for each class. Keys are integers
            0, 1, 2 ... for each corresponding class as well as "micro" and 
            "macro" for the averages.
        tpr (dict): the true positive rates for each class. Keys are integer 
            0, 1, 2, ... for each corresponding class as well "micro" and 
            "macro" for the averages.
        roc_auc (dict): the micro and macro average ROC-AUC score. Keys are
            "micro" and "macro"
            
    Code adapted from
    https://www.dlology.com/blog/simple-guide-on-how-to-generate-roc-plot-for-keras-classifier/
    
    Give the output of this function to the function plot_roc_auc() to plot 
    curves
    """
    one_hot_labels = one_hot_labels.astype(int)
    #Compute ROC curve and ROC area for each class
    n_classes = len(one_hot_labels[0])
    fpr = dict()
    tpr = dict()
    roc_auc = dict()
    for i in range(n_classes):
        fpr[i], tpr[i], thresholds = roc_curve(one_hot_labels[:, i],
                                               softmax_output[:, i])
        roc_auc[i] = auc(fpr[i], tpr[i])
    
    #Compute micro-average ROC curve and ROC area
    fpr["micro"], tpr["micro"], _ = roc_curve(one_hot_labels.ravel(),
                                              softmax_output.ravel())
    roc_auc["micro"] = auc(fpr["micro"], tpr["micro"])
    
    #Compute macro-average ROC curve and ROC area    
    #First aggregate all false positive rates
    all_fpr = np.unique(np.concatenate([fpr[i] for i in range(n_classes)]))
    
    #Then interpolate all ROC curves at this points
    mean_tpr = np.zeros_like(all_fpr)
    for i in range(n_classes):
        mean_tpr += interp(all_fpr, fpr[i], tpr[i])
    
    #Finally average it and compute AUC
    mean_tpr /= n_classes
    
    fpr["macro"] = all_fpr
    tpr["macro"] = mean_tpr
    roc_auc["macro"] = auc(fpr["macro"], tpr["macro"])
    
    return fpr, tpr, roc_auc

def ground_truth_comparison(settings, softmax_cube, thresholds, save_path=None,
                            dp=3, verbose=True):
    """
    FIXME: Clean up this function
    FIXME: Optimize this function
    Compares the predictions from a neural network to Kruse's 'ground truth'.

    Parameters:
        softmax_output ((m, n, o) ndarray): the softmax output from a neural
            network (non flattened version)
        save_path (str): where to save the output files
        dp (int): decimal point precision
        verbose (bool): whether to print function progress
        
    Returns:
        FIXME
    """
    assert type(softmax_cube) == np.ndarray, "softmax_cube must be of type ndarray"
    assert type(thresholds) in [list, np.ndarray], "thresholds must be of type list or ndarray"
    assert len(thresholds) > 0, "thresholds must be nonempty"
    for t in thresholds:
        assert 0 < t < 1, "each threshold must be between 0 and 1 exclusive"
    assert type(save_path) == str, "save_path must be of type str"
    
    #Get Kruse Ground Truth
    gt_labels = get_kruse_ground_truth(settings, dead=False)
    
    #Remove unclassified
    mask = gt_labels != settings.INT_MAP["Unclassified"]
    labels = gt_labels[mask]
    categories = settings.get_categories(labels=labels)
    for t in thresholds:
        if verbose:
            print("t={}".format(t))
        accs = []
        for cat in categories:
            #Get target integer
            tar_int = settings.INT_MAP[cat]
            
            #Get row/col locations in ground truth
            locs = np.argwhere(gt_labels==tar_int)
            rows, cols = zip(*locs)
            
            #Find Matches
            matches = softmax_cube[rows, cols, tar_int] >= t
            
            #Calculate Accuracy
            num = np.count_nonzero(matches)
            den = np.count_nonzero(gt_labels==tar_int)          
            acc = round(float(num/den), dp)
            accs.append(acc)
            
        #Calculate Overall Accuracy
        #Find all classified pixels
        locs = np.argwhere(np.logical_and(gt_labels != settings.INT_MAP["Dead"],
                                          gt_labels != settings.INT_MAP["Unclassified"]))
        rows, cols = zip(*locs)
        
        true_labels = gt_labels[rows, cols]
        pred_probs = softmax_cube[rows, cols, :]
        den = len(true_labels)
        num=0 
        #FIXME: eliminate for loop if possible
        for tl, pp in zip(true_labels, pred_probs):
            if pp[tl] > t:
                num += 1
        over_acc = round(float(num/den), dp)
        
        file_name = save_path + "_t={}".format(t)
        with open(file_name, "w") as f:
            f.write("Threshold={}\n".format(t))
            for cat, acc in zip(categories, accs):
                f.write("{}: {}\n".format(cat, acc))
            f.write("Overall: {}".format(over_acc))
            
        
        
        
    
#    #FIXME: this function needs to be updated
#    #FIXME: Check input
#    to_show = [list(categories[1:]) + [categories[0]] + ["Overall"]] #Put "unclassified" at bottom
#    headers = ["Material"]
#    if accuracy:
#        counts = [np.count_nonzero(ground_truth==i) for i in range(len(categories)+1)]
#        headers.append("Accuracy")
#        accs = []
#        
#        #Calcualte accuracy for all other labels
#        for cat in categories[1:]:
#            #Get target integer
#            tar_int = settings.INT_MAP[cat]
#            
#            #Get row/col locations in ground truth
#            locs = np.argwhere(ground_truth==tar_int)
#            rows, cols = zip(*locs)
#            
#            #Find Matches
#            tar_int -= 1 #Adjust to match softmax outputs
#            matches = softmax_output[rows, cols, tar_int] >= threshold
#            
#            #Calculate Accuracy
#            num = np.count_nonzero(matches)
#            den = counts[tar_int+1]            
#            acc = round(float(num/den), dp)
#            accs.append(acc)
#            
#        #Calculate accuracy for "Unclassified"
#        tar_int = settings.INT_MAP["Unclassified"]
#        #Get row/col locations in ground truth
#        locs = np.argwhere(ground_truth==tar_int)
#        rows, cols = zip(*locs)
#        
#        #Find matches
#        matches = softmax_output[rows, cols, :] < threshold
#        
#        #Calculate Accuracy
#        num = np.count_nonzero(np.apply_along_axis(np.all, axis=1, arr=matches))
#        den = counts[tar_int]
#        acc = round(float(num/den), dp)
#        accs.append(acc)
#        
#        #Calculate Overall Accuracy
#        #Find all "non-dead" pixels
#        locs = np.argwhere(ground_truth != settings.INT_MAP["Dead"])
#        rows, cols = zip(*locs)
#        
#        true_labels = ground_truth[rows, cols]
#        pred_probs = softmax_output[rows, cols, :]
#        
#        num = 0
#        for tl, pp in zip(true_labels, pred_probs):
#            if tl == settings.INT_MAP["Unclassified"]:
#                if np.all(pp < threshold):
#                    num += 1
#            else:
#                if pp[tl-1] >= threshold:
#                    num += 1
#        den = len(true_labels)
#        overall_acc = round(float(num/den), dp)    
#        accs.append(overall_acc)
#        to_show.append(accs)
#            
##    if precision:
##        headers.append("Precision")
##        precs = []
##    if recall:
##        headers.append("Recall")
##        recs = []
##    if f1:
##        headers.append("F1")
##        f1s = []
#    
#    data = list(map(list, zip(*to_show)))
#    
#    table = tabulate(data, headers=headers)
#    if save_path is not None:
#        with open(save_path, "w") as file:
#            file.write(table)
#    return table

def plot_pr_curves(settings,
                   one_hot_labels,
                   softmax_output,
                   file_path=None,
                   title=None):
    """
    Plot the Precision Recall (PR) curves for each class as well as the micro 
    average.
    
    Parameters: (as returned by get_auc_data())
        one_hot_labels ((m, n) ndarray): the true labels (in one-hot-encoded or 
            binary format). E.g. the labels [0, 1, 2] would become
            [[1, 0, 0], [0, 1, 0], [0, 0, 1]]        
        softmax_output ((m, n) ndarray): the softmax_output from a neural
            network classifier.
        file_name (str) optional: the name of the file to save the plot to. 
        classes (list(str)): the names of the classes (in appropriate order)
        title (str) optional: the title to use for the plot. If None, then the
            title is set to 'PR Curves'.
        
    See also get_roc_auc_data()

    Code adapted from
    https://scikit-learn.org/stable/auto_examples/model_selection/plot_precision_recall.html#in-multi-label-settings
    """
    #Check input
    assert type(one_hot_labels) == np.ndarray, "one_hot_labels must be of type ndarray"
    assert type(softmax_output) == np.ndarray, "tpr must be of type dict"
    assert type(file_path) == str or file_path is None, "file_name must be of type None or str"
    assert type(title) == str or title is None, "title must be of type str or be None"
    
    #Make sure labels and categories are aligned
    categories = settings.get_categories(labels=np.argmax(one_hot_labels, axis=1))
    num_cats = len(categories)
    assert num_cats == len(one_hot_labels[0]), "one_hot_labels and classes are not aligned"
    
    #Plot linewidth
    LW = 2
    #Get PR data
    precs, recs, aucs = get_pr_data(one_hot_labels, softmax_output)
    
    plt.figure(figsize=(7, 8))
    
    #Plot f1 curves
    f_scores = np.linspace(0.2, 0.8, num=4)
    lines = []
    labels = []
    for f_score in f_scores:
        x = np.linspace(0.01, 1)
        y = f_score * x / (2 * x - f_score)
        l, = plt.plot(x[y >= 0], y[y >= 0], color='gray', alpha=0.2)
        plt.annotate('f1={0:0.1f}'.format(f_score), xy=(.8, y[45] + 0.02))
    
    lines.append(l)
    labels.append('f1 countour lines')
    l, = plt.plot(recs["micro"], precs["micro"], color='deeppink', linestyle=':', lw=LW)
    lines.append(l)
    labels.append('micro-average (area = {0:0.2f})'.format(aucs["micro"]))
    
    for i, cat in zip(range(num_cats), categories):
        color = settings.COLOR_MAP[cat]
        l, = plt.plot(recs[i], precs[i], color=color, lw=LW)
        lines.append(l)
        labels.append('{0} (area = {1:0.2f})'.format(cat, aucs[i]))
    
    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.40)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('Recall')
    plt.ylabel('Precision')
    if title:
        plt.title(title)
    else:
        plt.title('PR Curves')
    plt.legend(lines, labels, loc=(0, -.8), prop=dict(size=14)) 
    plt.axes().set_aspect("equal")
    
    if file_path:
        plt.savefig(file_path)
    else:
        plt.show()
    plt.close()

def plot_roc_curves(settings,
                    one_hot_labels,
                    softmax_output, 
                    file_path=None,
                    title=None):
    """
    Plot the Reciever Operation Characteristic (ROC) curves for each class as
    well as the micro and macro averages.
    
    Parameters:
        one_hot_labels ((m, n) ndarray): the true labels (in one-hot-encoded or 
            binary format). E.g. the labels [0, 1, 2] would become
            [[1, 0, 0], [0, 1, 0], [0, 0, 1]]        
        softmax_output ((m, n) ndarray): the softmax_output from a neural
            network classifier.
        file_name (str) optional: the name of the file to save the plot to.
        
    See also get_roc_auc_data()
    """
    #Check input
    assert type(one_hot_labels) == np.ndarray, "one_hot_labels must be of type ndarray"
    assert type(softmax_output) == np.ndarray, "tpr must be of type dict"
    assert type(file_path) == str or file_path is None, "file_name must be of type None or str"
    
    
    #Make sure labels and classes are aligned
    categories = settings.get_categories(labels=np.argmax(one_hot_labels, axis=1))
    num_cats = len(categories)
    assert num_cats == len(one_hot_labels[0]), "one_hot_labels and classes are not aligned"
    
    #Plot linewidth
    LW = 2
    plt.figure(figsize=(7, 8))
    plt.axes().set_aspect("equal")
    #Get ROC/AUC Data
    fpr, tpr, roc_auc = get_roc_auc_data(one_hot_labels, softmax_output)
    # Plot all ROC curves
    lines = []
    labels = []
    l, = plt.plot(fpr["micro"], tpr["micro"],
                    color='deeppink', linestyle=':', linewidth=LW)
    lines.append(l)
    labels.append('micro-average (area = {0:0.2f})'.format(roc_auc["micro"]))
    l, = plt.plot(fpr["macro"], tpr["macro"],
                    color='navy', linestyle=':', linewidth=LW)
    lines.append(l)
    labels.append('macro-average (area = {0:0.2f})'.format(roc_auc["macro"]))
    
    for i, class_name in zip(range(num_cats), categories):
        color = settings.COLOR_MAP[class_name]
        l, = plt.plot(fpr[i], tpr[i], color=color, lw=LW)
        lines.append(l)
        labels.append('{0} (area = {1:0.2f})'.format(class_name, roc_auc[i]))
    
    plt.plot([0, 1], [0, 1], 'k--', lw=LW)

    fig = plt.gcf()
    fig.subplots_adjust(bottom=0.40)
    plt.xlim([0.0, 1.0])
    plt.ylim([0.0, 1.05])
    plt.xlabel('False Positive Rate')
    plt.ylabel('True Positive Rate')
    if title:
        plt.title(title)
    else:
        plt.title('ROC Curves')
    plt.legend(lines, labels, loc=(0, -.8), prop=dict(size=14))
        
    if file_path:
        plt.savefig(file_path, bbox_inches="tight")
    else:
        plt.show()
    plt.close()

def plot_training_curves(epochs, metrics, colors, labels, model_name=None, file_path=None):
    """Plots the training curves of a neural network classifier
    
    Parameters:
        epochs (list(int)): the epochs the metrics were evalutated at
        metrics (list(ndarray)): a list of the metrics to plot against epochs
            e.g. training loss, validation accuracy, etc.
        colors (list(str)): the colors corresponding to the metrics
        labels (list(str)): the metric labels
        model_name (str) optional: the name of the model. E.g. "Model1"
        file_path (str) optional: If a file_path is provided, the plot is saved. If not,
            the plot is not saved
    """
    
    plt.figure(figsize=(7, 8))
    #Plot each metric agains epochs
    for metric, color, label in zip(metrics, colors, labels):
        plt.plot(epochs, metric, color=color, label=label)
    plt.legend()
    
    #Label Axes
    plt.xlabel("Epochs")
    plt.ylabel("Metrics")
    
    #Plot title
    if model_name:
        title = model_name + " Training Curves"
    else:
        title = "Training Curves"
    plt.title(title)
    
    #Save plot
    if file_path:       
        plt.savefig(file_path)
    else:
        plt.show()
    plt.close()

def reliability_diagram(softmax_probs, target_labels, num_bins=10, title=None,
                        save_path=None):
    """
    Plot Reliability Diagram as described in the paper
    https://arxiv.org/pdf/1706.04599.pdf
    
    See Reliabilty Diagrams section
    
    Parameters:
        softmax_probs ((m, n), ndarray): the softmax output from a neural network
        target_labels ((m, ) ndarray): the "true" labels
            e.g. [0, 2, 1, 4]
        num_bins (int): the number of bins to use
        title (str) optional: The title of the plot. If None, the plot title is
            simply 'Reliability Diagram'
        save_path (str) optional: Where to save the plot. If None, the plot is
        shown using plt.show
    """
    #Check inputs
    assert type(softmax_probs) == np.ndarray, "softmax_probs must be of type np.ndarray"
    assert type(target_labels) == np.ndarray, "target_labels must be of type np.ndarray"
    assert type(num_bins) == int, "num_bins must be of type int"
    assert title is None or type(title) in [str, np.str_], "title must be None or of type str"
    assert save_path is None or type(save_path) in [str, np.str_], "save_path must be None or of type str"
    assert len(softmax_probs) == len(target_labels), "softmax_probs and target_labels are not aligned"    
    
    accs = []
    ave_confs = []
    counts = []
    for i in range(num_bins):
        #Identify confidence interval in softmax_probs
        lb = i/num_bins
        ub = (i+1)/num_bins
        locs = np.argwhere(np.logical_and(softmax_probs > lb, softmax_probs <= ub))
        if len(locs) == 0:
            accs.append(0)
            ave_confs.append(0)
            counts.append(0)
            continue
        rows, cols = zip(*locs)
        
        #Calculate accuracy
        den = len(rows)
        true_labels = target_labels[rows,]
        preds = np.array(cols)
        num = np.count_nonzero(true_labels == preds)
        acc = num/den
        accs.append(acc)
        counts.append(den)
        
        #Calculate average confidence
        ave_conf = np.mean(softmax_probs[rows, cols])
        ave_confs.append(ave_conf)
     
    #Calculate Expected Calibration Error (ECE) (equation (3) from paper)
    n = sum(counts)
    ECE = sum([Bm/n *abs(acc-conf) for Bm, acc, conf in zip(counts, accs, ave_confs)])
    #FIXME: the average confidence is below the output for higher confidence levels
    plt.figure(figsize=(5, 5))
    bar_width = 1/(num_bins)
    xs = np.linspace(0, (num_bins-1)/num_bins, num_bins)
    plt.bar(x=xs, height=ave_confs, width=bar_width, color="lightcoral", 
            align="edge", label="Gap", edgecolor="red")
    plt.bar(x=xs, height=accs, width=bar_width, color="steelblue", align="edge", 
            label="Outputs", edgecolor="black", alpha=1)
    plt.plot([0, 1], [0, 1], color="grey", linestyle="--", linewidth=3)
    plt.xlabel("Confidence")
    plt.ylabel("Accuracy")
    plt.legend(loc="upper left")
    textstr = "ECE = {:.3f}".format(ECE)
    ax = plt.gca()
    props = dict(boxstyle="round", facecolor="white", alpha=0.5)
    ax.text(0.5, 0.1,textstr, transform=ax.transAxes, bbox=props, fontsize=15)
    if title is not None:
        plt.title(title)
    else:
        plt.title("Reliability Diagram")
    
    if save_path is not None:
        plt.savefig(save_path)
    else:
        plt.show()
    plt.close()