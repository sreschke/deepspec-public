# -*- coding: utf-8 -*-
"""
visualize.py

7/10/19
Spencer Reschke
"""
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
import imageio
import random
import os
from deepspec.cuprite.post_training.inference import get_predictions
from deepspec.cuprite.utility import utils

def bar_chart(settings, labels, colors=None, title=None, save_path=None, count=False):
    """
    Creates a bar chart to visualize class distributions
    
    Parameters:
        settings (pipe_settings): a pipe_settings class instantiation
        labels ((m, NONE) ndarray): the labels corresponding to a spectral 
            image.
            E.g. a 2d integer array where each entry indicates a class/label
            E.g. a 1d integer array where each entry indicates a class/label 
        colors (list(str)) optional: The colors to use for each material. The
            colors must be pre-defined matplotlib colors. If None, the function
            uses the assigned colors from the settings file
        title (str) optional: The plot title. If None, no title is used.
        save_path (str) optional: the location to save the bar chart to. If
            None, the bar chart is displayed using plt.show()
            
    Future Work: add option to display class counts and total count in plot
    """
    #Check inputs
    assert type(labels) == np.ndarray, "labels must be of type ndarray"
    #Get categories
    categories = settings.get_categories(labels=labels)
    num_cats = len(categories)
    if colors is not None:
        assert len(colors) > 0, "colors cannot be empty"
        for col in colors:
            assert type(col) in [str, np.str_], "Each entry in colors must be of type string"
        assert len(categories) == len(colors), "categories and colors must have same length"
    else:
        colors = [settings.COLOR_MAP[cat] for cat in categories]
    
    #Count each category
    counts = [np.count_nonzero(labels == i) for i in range(num_cats)]
        
    #Creat Bar Chart
    xs = np.arange(num_cats)
    barlist=plt.bar(xs, counts)
    
    #Set Bar colors
    for i, cat, col in zip(range(num_cats), categories, colors):
        barlist[i].set_color(col)
    if count:
        categories = [cat + " ({})".format(cnt) for cat, cnt in zip(categories, counts)]
    plt.xticks(xs, categories, rotation='vertical')
    plt.ylabel("Count")
    if title:
        plt.title(title)   
    
    plt.tight_layout()
    #Save or show figure
    if save_path:
        plt.savefig(save_path)
        plt.close()
    else:
        plt.show()
        
def plot_pca_data(settings, x_pca, labels, save_path, sup_title=None):
    """
    Plot data after it's normalization and PCA
    
    Parameters:
        settings (pipe_settings): a pipe_settings class instantiation
        x_pca ((m, n) ndarray): the transformed spectra
        labels ((m, ) ndarray): the corresponding labels
        categories (list): the material types
        save_dir (str): the path to save the resulting plot to
    """
    #FIXME: check input
    
    #Get categories
    
    #Usefule variables
    categories = settings.get_categories(labels=labels)
    num_cats = len(categories)
    counts = [np.count_nonzero(labels == i) for i in np.unique(labels)]
    num_samples = np.min([20, np.min(counts)])    
    num_comps = len(x_pca[0])
    
    #Plot Samples
    avgs = []
    plt.figure(figsize=(7, 8))
    for i in range(num_cats):
        plt.subplot(3, 3, i+1)
        label = categories[i]
        population = x_pca[labels==i]
        idx = np.random.choice(population.shape[0], num_samples, replace=False)
        samples = population[idx]
        avgs.append(np.mean(samples, axis=0))
        for k in range(num_samples):
            plt.plot(samples[k, :])
        plt.title(label)
        plt.xlabel("Principle Components")
        plt.xticks([0, num_comps-1], [1, num_comps])
        plt.title(label)
    if sup_title:
        plt.suptitle(sup_title, size=16)
    plt.gcf().tight_layout(rect=[0, 0.03, 1, 0.95])
    plt.savefig(save_path)
    plt.close()    
        
def plot_spectral_image(settings, labels, colors=None, title=None,
                        save_path=None, get_img=False):
    """
    Plot a spectral image
    
    Parameters:
        settings (pipe_settings): a pipe_settings class instantiation
        labels ((m, n) ndarray): the labels of a spectral image (2d version). 
            Assumes zero indexing (so first class has label 0, second class has 
            label 1 and so on). E.g. a 2d integer array with each entry
            indicating a different material.
        colors (list(str)) optional: the colors to use for each category. The 
            colors need to be named matplotlib colors 
            (see https://matplotlib.org/examples/color/named_colors.html)
            If None, the colors assigned in settings are used.
        title (str) optional: the title to use for the resulting plot
        save_path (str) optional: the path to save the resulting plot to. If not given,
            the plot is shown instead of saved
        get_img (bool) optional: whether to return a rbg image of the resulting
            plot
        
    Generates:
        If save_path is specified, the resulting plot is saved to that path as 
            a png file.
        If get_img is true, the resulting plot is converted to a rbg imgage and
            is returned as a numpy array.
        If save_path is None and get_img is False, the resulting plot is shown
            by calling plt.show()
    """
    #Check input
    assert type(labels) == np.ndarray, "labels must be of type ndarray"
    categories = settings.get_categories(labels=labels)
    num_cats = len(categories)    
    if colors is not None:
        assert type(colors) in [list, np.ndarray], "colors must be of type list or ndarray"
        assert len(categories) == len(colors), "colors and categories must have same length"
    else:
        colors = [settings.COLOR_MAP[cat] for cat in categories]
    num_colors = len(colors)    
    assert num_cats==num_colors, "categories and colors must have the same length"
    if title is not None:
        assert type(title) == str, "title must be of type str"
    if save_path is not None:
        assert type(save_path) == str, "save_path must be of type str"
      
    plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
    cmap = matplotlib.colors.ListedColormap(colors)
    plt.imshow(labels, cmap=cmap, vmin=0, vmax=num_cats-1)
    ax = plt.colorbar()
    scale = (num_cats-1)/num_cats
    shift = (num_cats-1)/(2*num_cats)
    ax.set_ticks(np.arange(num_cats)*scale + shift)
    ax.set_ticklabels(categories)
    plt.title("Cuprite Classified MSI")
    plt.tight_layout()
    
    #Set Title
    if title is not None:
        plt.title(title)
    else:
        plt.title("Spectral Image")
    #FIXME label axes
    if save_path:
        plt.savefig(save_path)
        plt.close()
    elif get_img:
        fig = plt.gcf()
        #Get corresponding image array
        fig.canvas.draw()
        image = np.frombuffer(fig.canvas.tostring_rgb(), dtype="uint8")
        image = image.reshape(fig.canvas.get_width_height()[::-1] + (3,))
        plt.close()
        return image
    else:
        plt.show()
        plt.close()
        
def spectral_gif(settings, predictions, title=None, save_path=None, fps=1):
    """
    FIXME: clean up this function
    
    Makes a gif of the predictions
    Parameters:
        predictiosn ((m, n, o) ndarray): the class predictions
        ...
        FIXME
    """
    #FIXME: check input
    #Wrapper function
    def get_img_array(labels=predictions, title=title):
        return plot_spectral_image(settings=settings, labels=labels, title=title, get_img=True)
        
    #Get number of layers to use
    num_layers = 0
    unc_int = settings.INT_MAP["Unclassified"]
    dead_int = settings.INT_MAP["Dead"]
    for i in range(predictions.shape[2]):
        if np.count_nonzero(np.logical_and(predictions[:, :, i] != unc_int, predictions[:, :, i] != dead_int)) > 0:
            num_layers += 1
            
    #Get arrs
    if num_layers > 0:
        arrs = [get_img_array(predictions[:, :, i], title="prediction {}".format(i+1)) for i in range(num_layers)]
    else:
        arrs = [get_img_array(predictions[:, :, 0], title="prediction {}".format(1))]
        
    #Using imageio
    imageio.mimsave(save_path, arrs, fps=fps)

    
def plot_spectra_samples(settings, spectra, labels, title=None, save_path=None,
                         num_samples=20, ave=False, verbose=True):    
    """    
    Randomly sample and plot spectra
    Samples unprocessed spectra as well as their averages (in separate plots)
    
    Parameters:
        settings (pipe_settings): a pipe_settings class instantiation
        spectra ((m, n, o) ndarray): a spectral cube
        labels: ((m, n) ndarray): the spectral labels
        title: (str) optional: the title to use on the plot
        save_path (str) optional: Where to save the plots. If None, the plots
            are not saved
        num_samples (int): how many samples to use for each category
        ave (bool): whether to print the averages
        verbose (bool): whether to display function progress
    """
    #FIXME: Check inputs
    categories = settings.get_categories(labels=labels)
    num_cats = len(categories)
    
    #Visualize spectra for each material
    #Randomly sample spectra for each type of material and plot
    if verbose:
        print("Plotting Sample Spectra...")
    plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
    avgs = []
    
    #Plot Samples
    num_cols = 3
    num_rows = int(np.ceil((num_cats/num_cols)))
    for i in range(num_cats):
        plt.subplot(num_rows, num_cols, i+1)
        locs = random.sample([list(j) for j in np.argwhere(labels==i)], k=num_samples)
        rows, cols = zip(*locs)
        label = categories[i]
        samples = spectra[rows, cols, :]
        avgs.append(np.mean(samples, axis=0))
        for k in range(num_samples):
            plt.plot(samples[k, :])
        plt.title(label)
        plt.xlabel("Wavelength Band")
        plt.ylabel("Reflectance")

    if title is not None:
        plt.suptitle(title, y=.91, size=16)
    plt.tight_layout()
    plt.subplots_adjust(top=0.82)
    
    if verbose:
        print("Saving samples...")
    if save_path is not None:   
        plt.savefig(save_path)
        plt.close()
    
    if ave:
        if verbose:
            print("Plotting Averages...")
        plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
        for i in range(num_cats):
            label = categories[i]
            color = settings.COLOR_MAP[label]
            avg_spectra = avgs[i]
            plt.plot(avg_spectra, color=color, label=label)
            
        #Shrink current axis by 20%
        ax = plt.gca()
        box = ax.get_position()
        ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        
        # Put a legend to the right of the current axis
        ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        plt.title("Unprocessed Spectral Averages")
        plt.xlabel("Wavelength Band")
        plt.ylabel("Reflectance")
        
        if verbose:
            print("Saving Averages...")
        if save_path is not None:
            save_path += "_avg"
            plt.savefig(save_path)
            plt.close()
            
def visualize_predictions(settings, softmax_cube, thresholds=None,
                          save_dir=None, verbose=True):
    """
    Creates a gif 
    FIXME: we might want to break this function up
    
    Parameters:
        settings (pipe_settings): a pipe_settings instantiation
        softmax_cube ((m, n, o) ndarray) optional: The class distributions for
            each pixel.
        labels ((m, n) ndarray): the spectral labels (integers with zero indexing)
        thresholds (list(floats)): what threshold(s) to use. The thresholds
            must be between 0 and 1 exclusive.
        save_dir (str): directory to save the output files to
         
        verbose (bool): whether to print function progress
    
    Returns:
        FIXME
    """
    
    #Check input
    assert type(softmax_cube) == np.ndarray, "softmax_cube must be of type ndarray"
    assert type(save_dir) == str, "save_dir must be of type str"
    assert type(thresholds) in [list, np.ndarray], "thresholds must be of type list or ndarray"
    
    #Find "Dead" pixels
    mask = softmax_cube[:, :, 0] < 0
    num_deads = np.count_nonzero(mask)
    
    #Predict with thresholding
    for t in thresholds:
        if verbose:
            print("t={}...".format(t))
        predictions = get_predictions(settings=settings,
                                      softmax_cube=softmax_cube,
                                      threshold=t)
        if num_deads > 0:
            predictions[mask] = settings.INT_MAP[utils.DEAD_NAME]
        title = "t={:.2f}".format(t)
        save_path = save_dir + os.sep + "t={:.6f}_predictions.gif".format(t)
        
        spectral_gif(settings=settings,
                     predictions=predictions, 
                     title=title,
                     save_path=save_path)