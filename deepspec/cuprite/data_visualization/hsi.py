# -*- coding: utf-8 -*-
"""
hsi.py
7/10/19

Spencer Reschke
"""
import numpy as np
import matplotlib.pyplot as plt
import matplotlib.gridspec as gridspec
import random

def processed_samples(settings, spectral_cube, labels, wavelengths, title,
                      num_samples=20, save_path=None, ave=True, verbose=True):
    """
    Sample and plot processed spectra
    
    FIXME:
    Parameters:
        settings (settings): a settings class instantiation
        specrtra ((m, n, o) ndarray):
        labels ((m, n) ndarray):
        wavelengths (() ndarray):
        title (str): The plot title to use
        num_samples (int): how many samples to use for each category
        save_path (str) optional: where to save the plots
        ave (bool): whether to plot the sample averages. If true, a separate
            plot is made.
        verbose (bool): whether to print function progress
    """
    assert type(spectral_cube) == np.ndarray, "spectra must be of type ndarray"
    assert type(labels) == np.ndarray, "labels must be of type ndarray"
    assert type(wavelengths) == np.ndarray, "wavelenghts must be of type ndarray"
    assert type(title) == str, "title must be of type str"
    assert type(num_samples) == int, "num_samples must be of type ndarray"
    assert type(save_path) == str, "save_path must be of type str"
    assert type(verbose) == bool, "verbose must be of type bool"
    #FIXME: check input shapes
    #FIXME: this function throws a matplotlib warning:
    #   UserWarning: This figure includes Axes that are not compatible with tight_layout, so results might be incorrect.

    categories = settings.get_categories(labels=labels)
    num_cats = len(categories)
    
    #Get samples from each categorie and plot them
    fig = plt.figure(figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
    outer = gridspec.GridSpec(3, 3, wspace=0.2, hspace=0.4)
    avgs = []
    
    print("Plotting Samples...")
    for i in range(num_cats):
        
        inner = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec=outer[i], wspace=0.4, hspace=0.1)
        
        #Get samples
        #For 3d version
        samples = random.sample(list(spectral_cube[labels==i]), k=num_samples)
        label = categories[i]
#        #For 2d version
#        locs = random.sample(list(np.argwhere(labels==i).flatten()), k=num_samples)
#        label = categories[i]
#        samples = spectra[locs]
        
        #Take average of samples
        #FIXME: remove hand-coded numbers
        avgs.append(np.mean(samples, axis=0))
        for j in range(3):
            ax = plt.Subplot(fig, inner[j])
            if j == 0:
                for y in samples:
                    if i % 3 != 0:
                        plt.setp(ax.get_yticklabels(), visible=False)
                    else:
                        #FIXME set y label
                        pass
                    ax.plot(wavelengths, y)
                    ax.set_xlim(0,99)
                    ax.spines['right'].set_visible(False)
                    ax.yaxis.tick_left()
                    ax.tick_params(labeltop=False)
                    #kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
                    ax.set_xticks([0, 50, 99])
            elif j == 1:
                for y in samples:
                    ax.plot(wavelengths, y)
                    ax.set_xlim(111, 144)
                    ax.spines['left'].set_visible(False)
                    ax.spines['right'].set_visible(False)
                    ax.yaxis.tick_left()
                    ax.set_xticks([111, 125, 144])
                    plt.setp(ax.get_yticklabels(), visible=False)
                    ax.set_title(label)
            else:
                for y in samples:
                    ax.plot(wavelengths, y)
                    ax.set_xlim(174, 223)
                    ax.spines['left'].set_visible(False)
                    ax.yaxis.tick_left()
                    ax.set_xticks([174, 200, 223])
                    plt.setp(ax.get_yticklabels(), visible=False)
            ax.tick_params(axis="x", which="major", labelsize=8)
            fig.add_subplot(ax)
        
    #FIXME: figure out where to add x label
    #Finishing touches on figure
#    sub_text = "Notes: \n\
#    processed data: absorption lines removed\n\
#    {} samples each \n\
#    Wavelengths (in micrometers) are grouped into\n\
#    about 220 bands".format(num_samples).expandtabs()
    plt.suptitle(title, y=.91, size=16)
#    plt.figtext(s=sub_text.format(num_samples), x=0.40, y=.12, size=16)
    plt.tight_layout()
    plt.subplots_adjust(top=0.82)
    
    if save_path is not None:
        #Save figure
        plt.savefig(save_path)
        plt.close()
    if ave:
        print("Plotting Averages...")
        fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
        fig.set_size_inches(9, 7)
        
        #Plot same data on all axes
        for i in range(num_cats):
            avg_spec = avgs[i]
            label = categories[i]
            color = settings.COLOR_MAP[label]
            ax1.plot(wavelengths, avg_spec, color=color)
            ax2.plot(wavelengths, avg_spec, color=color)
            ax3.plot(wavelengths, avg_spec, color=color, label=label)
            
        #Zoom-in / limit the view to different portions of the data
        ax1.set_xlim(0,99)
        ax2.set_xlim(111, 144)
        ax3.set_xlim(174, 223)
        
        #Hide Spines
        ax1.spines['right'].set_visible(False)
        ax2.spines['left'].set_visible(False)
        ax2.spines['right'].set_visible(False)
        ax3.spines['left'].set_visible(False)
        
        #Set tick marks
        ax1.set_xticks([0, 50, 99])
        ax2.set_xticks([111, 125, 144])
        ax3.set_xticks([174, 200, 223])
        
        #Shrink current axis by 20%
        ax3 = plt.gca()
        box = ax3.get_position()
        ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])
        
        #Put a legend to the right of the current axis
        ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5))
        
        #Make Title
        plt.subplots_adjust(top=0.94, right=0.8)
        plt.suptitle("Processed Sample Averages", y=.98, size=16)
        
        if save_path is not None:
            print("Saving Averages...")
            save_path += "_avg"
            plt.savefig(save_path)
            plt.close()