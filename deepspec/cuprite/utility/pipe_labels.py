# -*- coding: utf-8 -*-
import numpy as np
from matplotlib import colors as mcolors
import matplotlib.pyplot as plt
import matplotlib
from deepspec.cuprite.utility import utils
from deepspec.cuprite.utility.pipe_settings import pipe_settings
from collections.abc import Iterable
import random
import matplotlib.gridspec as gridspec
from sklearn.model_selection import train_test_split

class pipe_labels():
    def __init__(self, categories, colors, labels, name):
        """Constructor"""        
        #Check input
        assert len(categories) == len(colors), "categories and colors must have the same length"
        
        #Check categories
        for cat in categories:
            assert type(cat) in [str, np.str_], "Each category must be of type string"
            
        assert utils.UNCLASS_NAME in categories, "{} must be in categories".format(utils.UNCLASS_NAME)
            
        #Get named matplotlib colors
        cols = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
        by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                        for name, color in cols.items())
        named_colors = [name for hsv, name in by_hsv]
        
        #Check colors
        for col in colors:
            assert type(col) in [str, np.str_], "Each color must be of type string"
            assert col in named_colors, "Each color must be named in matplotlib; please see https://matplotlib.org/examples/color/named_colors.html"
        
        #Check labels
        assert type(labels) == np.ndarray, "labels must be of type ndarray"
        assert labels.ndim == 2, "labels should be two dimensional"
        
        #Check name
        assert type(name) in [str, np.str_], "name must be of type str"
        
        
        #Used to align labels later
        old_int_map = dict(zip(categories, np.arange(len(categories))))
        
        #alphabetize categories and colors
        categories = np.array(categories)
        colors = np.array(colors)
        args = categories.argsort()
        categories = categories[args]
        colors = colors[args] 

        #Put "Dead" and "Unclassified" last
        unc_mask = categories == utils.UNCLASS_NAME
        categories = np.concatenate([categories[~unc_mask], categories[unc_mask]])
        colors =  np.concatenate([colors[~unc_mask], colors[unc_mask]])
        
        if utils.DEAD_NAME in categories:
            dead_mask = categories == utils.DEAD_NAME
            categories = np.concatenate([categories[~dead_mask], categories[dead_mask]])
            colors =  np.concatenate([colors[~dead_mask], colors[dead_mask]])

        
        #Make sure 'Dead' and 'Unclassified' come last
        if utils.DEAD_NAME in categories:
            assert utils.DEAD_NAME == categories[-1]
            assert utils.UNCLASS_NAME == categories[-2]
        else:
            assert utils.UNCLASS_NAME == categories[-1]
        
        
        #Set class members
        self._CATEGORIES = categories
        self._COLORS = colors
        self._construct_maps(self._CATEGORIES, self._COLORS)
        self._LABELS = self._align_labels(old_int_map, labels)
        self._NAME = name
        
    
    def _align_labels(self, old_int_map, old_labels):
        """
        Aligns labels with INT_MAP
        
        Parameters:
            old_int_map (dict): the old material name to integer mapping
            old_labels ((m, n) ndarray)): the labels to line up
        
        Returns:
            ((m, n) ndarray): the aligned labels
        """
        #Match labels with settings file
        al_labels = old_labels.copy()
        for cat in old_int_map.keys():
            search_int = old_int_map[cat]
            mask = old_labels == search_int
            if cat in self._CATEGORIES:
                tar_int = self._INT_MAP[cat]
            else:
                tar_int = self._INT_MAP[utils.UNCLASS_NAME]
            al_labels[mask] = tar_int
        return al_labels
    
    
    def bar_chart(self, count=False, live_only=False, save_path=None):
        """
        Creates a bar chart showing the category distributions
        """
        categories = self._CATEGORIES.copy()
        if live_only:
            if utils.DEAD_NAME in categories:
                categories = categories[categories != utils.DEAD_NAME]
            if utils.UNCLASS_NAME in categories:
                categories = categories[categories != utils.UNCLASS_NAME]
        num_cats = len(categories)
        #Count each category
        counts = [np.count_nonzero(self._LABELS == self._INT_MAP[cat]) for cat in categories]
            
        #Creat Bar Chart
        xs = np.arange(num_cats)
        barlist=plt.bar(xs, counts)
        
        #Set Bar colors
        for i, cat, col in zip(range(num_cats), self._CATEGORIES, self._COLORS):
            barlist[i].set_color(col)
        if count:
            categories = [cat + " ({})".format(cnt) for cat, cnt in zip(self._CATEGORIES, counts)]
        else:
            categories = self._CATEGORIES
        
        plt.xticks(xs, categories, rotation='vertical')
        plt.ylabel("Count")
        plt.title(self._NAME)   
        
        plt.tight_layout()
        #Save or show figure
        if save_path:
            plt.savefig(save_path)
            plt.close()
        else:
            plt.show()
    
        
    def _construct_maps(self, categories, colors):
        """
        Helper function
        
        Constructs the class members INT_MAP and COLOR_MAP. Called from
        constructor and remove().
        
        Parameters:
            FIXME:               
        """
        assert len(categories) == len(colors), "categories and colors must have the same length"
        self._INT_MAP = dict(zip(categories, range(len(categories))))
        self._COLOR_MAP = dict(zip(categories, colors))
        
        
    def copy(self, new_name=None):
        """Creates a copy of the pipe_labels instantiation
        
        Parameters:
            new_name (str) optional: the name of the copy. If None, the name 
                from the original is used.
        
        Returns:
            pipe_labels_copy (pipe_labels): the copy
        """
        if new_name is None:
            pipe_labels_copy = pipe_labels(categories=self._CATEGORIES.copy(),
                                           colors=self._COLORS.copy(),
                                           labels=self._LABELS.copy(),
                                           name=self._NAME)
        else:
            pipe_labels_copy = pipe_labels(categories=self._CATEGORIES.copy(),
                                           colors=self._COLORS.copy(),
                                           labels=self._LABELS.copy(),
                                           name=new_name)
        return pipe_labels_copy
        
        
    def flatten(self, spectral_cube):
        """Flattens a spectral_cube by selecting all categorized spectra (all
        spectra that are not 'dead' or 'unclassified'). Returns the corresponding
        integer labels and settings.
        
        Parameters:
            spectral_cube ((m, n, o), ndarray): a cuprite spectral cube
            
        Returns:
            ((p, o) ndarray): the flattened spectra
            ((p, ) ndarray): the flattend labels
            (pipe_settings): a pipe_settings class instantiation
            
            
        """
        #check input
        assert type(spectral_cube) == np.ndarray, "spectral_cube must be of type ndarray"
        assert spectral_cube.ndim == 3, "spectral_cube must be three dimensional"
        
        mask = self.get_live_mask()
        return_labels = self._LABELS[mask]
        temp = np.unique(return_labels)
        temp2 = len(temp)
        assert np.all(np.arange(temp2) == temp), "FIXME: The return_labels are not aligned. Complain to Spencer"
        
        settings = self.get_settings()
        
        return spectral_cube[mask], return_labels, settings
    
    
    def get_live_categories(self):
        """Retrieves the categories that are not 'dead' or 'unclassified'
        
        Returns:
            ((n, ) ndarray): the 'live' categories
        """
        categories = self._CATEGORIES.copy()
        if utils.DEAD_NAME in categories:
            categories = categories[categories != utils.DEAD_NAME]
        if utils.UNCLASS_NAME in categories:
            categories = categories[categories != utils.UNCLASS_NAME]
            
        return categories
    
    
    def get_live_mask(self):
        """
        Creates a numpy boolean mask where the labels are not 'Dead' or
        'Unclassified'
        
        Returns:
            mask ((m, n) ndarray): the boolean mask
        """
        assert utils.UNCLASS_NAME in self._CATEGORIES, "'Unclassified' should be a category"
        
        if utils.DEAD_NAME in self._CATEGORIES:
            mask = np.logical_and(self._LABELS != self._INT_MAP[utils.DEAD_NAME],
                                  self._LABELS != self._INT_MAP[utils.UNCLASS_NAME])
        else:
            mask = self._LABELS != self._INT_MAP[utils.UNCLASS_NAME]
        return mask
    
    
    def get_settings(self):
        """Retrieves a pipe_settings instance corresponding to the pipe_labels
        instance. 
        
        Returns: 
            (pipe_settings)
        """
        return pipe_settings(self._CATEGORIES, self._COLORS)


    def plot_spectra_samples(self, spectral_cube, save_path=None,
                         num_samples=20, ave=False):    
        """    
        Randomly sample and plot spectra
        Samples unprocessed spectra as well as their averages (in separate plots)
        
        Parameters:
            spectral_cube ((m, n, o) ndarray): a spectral cube
            save_path (str) optional: Where to save the plots. If None, the plots
                are not saved
            num_samples (int): how many samples to use for each category
            ave (bool): whether to print the averages
            verbose (bool): whether to display function progress
        """
        #FIXME: Check inputs
        #FIXME: Make sure num_samples isn't too large
        
        assert spectral_cube.ndim == 3, "spectral_cube must be 3 dimensional"
        num_cats = len(self._CATEGORIES)
        
        #Visualize spectra for each material
        #Randomly sample spectra for each type of material and plot
        plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
        avgs = []
        
        #Plot Samples
        num_cols = 3
        num_rows = int(np.ceil((num_cats/num_cols)))
        for i in range(num_cats):
            plt.subplot(num_rows, num_cols, i+1)
            locs = random.sample([list(j) for j in np.argwhere(self._LABELS==i)], k=num_samples)
            rows, cols = zip(*locs)
            label = self._CATEGORIES[i]
            samples = spectral_cube[rows, cols, :]
            avgs.append(np.mean(samples, axis=0))
            for k in range(num_samples):
                plt.plot(samples[k, :])
            plt.title(label)
            plt.xlabel("Wavelength Band")
            plt.ylabel("Reflectance")
    
        plt.suptitle("Samples", y=.91, size=16)
        plt.tight_layout()
        plt.subplots_adjust(top=0.82)
        
        if save_path is not None: 
            plt.savefig(save_path)
            plt.close()
        else:
            plt.show()
        
        if ave:
            plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
            for i in range(num_cats):
                label = self._CATEGORIES[i]
                color = self._COLOR_MAP[label]
                avg_spectra = avgs[i]
                plt.plot(avg_spectra, color=color, label=label)
                
            #Shrink current axis by 20%
            ax = plt.gca()
            box = ax.get_position()
            ax.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            
            # Put a legend to the right of the current axis
            ax.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            plt.title("Sample Averages")
            plt.xlabel("Wavelength Band")
            plt.ylabel("Reflectance")
            
            if save_path is not None:
                save_path += "_avg"
                plt.savefig(save_path)
                plt.close()
            else:
                plt.show()
                
                
    def plot_processed_spectral_samples(self, spectral_cube, indices,
                                        num_samples=20, save_path=None,
                                        ave=True, verbose=True):
        """
        Sample and plot processed spectra
        
        FIXME:
        Parameters:
            specrtral_cube ((m, n, o) ndarray):
            indices (() ndarray):
            num_samples (int): how many samples to use for each category
            save_path (str) optional: where to save the plots
            ave (bool): whether to plot the sample averages. If true, a separate
                plot is made.
            verbose (bool): whether to print function progress
        """
        assert type(spectral_cube) == np.ndarray, "spectra must be of type ndarray"
        assert type(num_samples) == int, "num_samples must be of type int"
        if save_path is not None:
            assert type(save_path) in [str, np.str_], "save_path must be of type str"
        assert type(ave) == bool, "ave must be of type bool"
        assert type(verbose) == bool, "verbose must be of type bool"
        assert spectral_cube.ndim == 3, "spectral_cube must be three dimensional"
        #FIXME: check input shapes
        #FIXME: this function throws a matplotlib warning:
        #   UserWarning: This figure includes Axes that are not compatible with tight_layout, so results might be incorrect.
        num_cats = len(self._CATEGORIES)
        
        #Get samples from each categorie and plot them
        fig = plt.figure(figsize=(10, 8), dpi=80, facecolor="w", edgecolor="k")
        outer = gridspec.GridSpec(3, 3, wspace=0.2, hspace=0.4)
        avgs = []
        
        print("Plotting Samples...")
        for i in range(num_cats):
            
            inner = gridspec.GridSpecFromSubplotSpec(1, 3, subplot_spec=outer[i], wspace=0.4, hspace=0.1)
            
            #Get samples
            samples = random.sample(list(spectral_cube[self._LABELS==i]), k=num_samples)
            cat = self._CATEGORIES[i]
            
            #Take average of samples
            #FIXME: remove hand-coded numbers
            avgs.append(np.mean(samples, axis=0))
            for j in range(3):
                ax = plt.Subplot(fig, inner[j])
                if j == 0:
                    for y in samples:
                        if i % 3 != 0:
                            plt.setp(ax.get_yticklabels(), visible=False)
                        else:
                            #FIXME set y label
                            pass
                        ax.plot(indices, y)
                        ax.set_xlim(0,99)
                        ax.spines['right'].set_visible(False)
                        ax.yaxis.tick_left()
                        ax.tick_params(labeltop=False)
                        #kwargs = dict(transform=ax.transAxes, color='k', clip_on=False)
                        ax.set_xticks([0, 50, 99])
                elif j == 1:
                    for y in samples:
                        ax.plot(indices, y)
                        ax.set_xlim(111, 144)
                        ax.spines['left'].set_visible(False)
                        ax.spines['right'].set_visible(False)
                        ax.yaxis.tick_left()
                        ax.set_xticks([111, 125, 144])
                        plt.setp(ax.get_yticklabels(), visible=False)
                        ax.set_title(cat)
                else:
                    for y in samples:
                        ax.plot(indices, y)
                        ax.set_xlim(174, 223)
                        ax.spines['left'].set_visible(False)
                        ax.yaxis.tick_left()
                        ax.set_xticks([174, 200, 223])
                        plt.setp(ax.get_yticklabels(), visible=False)
                ax.tick_params(axis="x", which="major", labelsize=8)
                fig.add_subplot(ax)
            
        #FIXME: figure out where to add x label
        #Finishing touches on figure
    #    sub_text = "Notes: \n\
    #    processed data: absorption lines removed\n\
    #    {} samples each \n\
    #    indices (in micrometers) are grouped into\n\
    #    about 220 bands".format(num_samples).expandtabs()
        plt.suptitle("Processed Samples", y=.91, size=16)
    #    plt.figtext(s=sub_text.format(num_samples), x=0.40, y=.12, size=16)
        plt.tight_layout()
        plt.subplots_adjust(top=0.82)
        
        if save_path is not None:
            #Save figure
            plt.savefig(save_path)
            plt.close()
        else:
            plt.show()
        if ave:
            print("Plotting Averages...")
            fig, (ax1, ax2, ax3) = plt.subplots(1, 3, sharey=True)
            fig.set_size_inches(9, 7)
            
            #Plot same data on all axes
            for i in range(num_cats):
                avg_spec = avgs[i]
                cat = self._CATEGORIES[i]
                color = self._COLOR_MAP[cat]
                ax1.plot(indices, avg_spec, color=color)
                ax2.plot(indices, avg_spec, color=color)
                ax3.plot(indices, avg_spec, color=color, label=cat)
                
            #Zoom-in / limit the view to different portions of the data
            ax1.set_xlim(0,99)
            ax1.set_ylabel("Reflectance")
            ax2.set_xlim(111, 144)
            ax2.set_xlabel("Wavelength Band")
            ax3.set_xlim(174, 223)
            
            #Hide Spines
            ax1.spines['right'].set_visible(False)
            ax2.spines['left'].set_visible(False)
            ax2.spines['right'].set_visible(False)
            ax3.spines['left'].set_visible(False)
            
            #Set tick marks
            ax1.set_xticks([0, 50, 99])
            ax2.set_xticks([111, 125, 144])
            ax3.set_xticks([174, 200, 223])
            
            #Shrink current axis by 20%
            ax3 = plt.gca()
            box = ax3.get_position()
            ax3.set_position([box.x0, box.y0, box.width * 0.8, box.height])
            
            #Put a legend to the right of the current axis
            ax3.legend(loc='center left', bbox_to_anchor=(1, 0.5))
            
            #Make Title
            plt.subplots_adjust(top=0.94, right=0.8)
            plt.suptitle("Processed Averages", y=.98, size=16)
            
            if save_path is not None:
                print("Saving Averages...")
                save_path += "_avg"
                plt.savefig(save_path)
                plt.close()
            else:
                plt.show()
                
                
    def random_sample(self, count=None, per=None, name="Kruse Sample"):
        """
        FIXME: write description
        If per is not None, sample will have same distribution as original
        If count is not None, sample will have uniform distribution
        
        Parameters:
            settings (pipe_settings): a pipe_settings instantiation
            labels ((m, n) ndarray): the spectral labels
            per (float): the percentage of data to use in the sample
            name (str): what to name the sample
            
        Returns:
            (pipe_labels): a pipe_labels instance       
        """
        #Check input
        unique = np.unique(self._LABELS)
        if per is not None:        
            assert 0 < per < 1, "per must be between 0 and 1"
        if count is not None:
            #Make sure there are enough samples in each category
            counts = np.array([np.count_nonzero(self._LABELS == i) for i in unique])
            assert np.all(count < counts), "There are not {} samples for each material. Counts: {}".format(count, counts)
        assert per is not None or count is not None, "per and count cannot both be None"
        assert type(name) in [str, np.str_], "name must be of type str"
            
        mask = np.zeros((utils.NUM_ROWS, utils.NUM_COLS), dtype=bool)          
        #Sample from each category
        for cat in self._CATEGORIES:
            tar_int = self._INT_MAP[cat]
            locs = [list(j) for j in np.argwhere(self._LABELS==tar_int)]
            rows, cols = zip(*locs)
            rows = np.array(rows)
            cols = np.array(cols)
            if per is not None:
                rows, _, cols, _ = train_test_split(rows, cols, train_size=per)
            elif count is not None:
                idx = np.random.choice(a=np.arange(len(rows)), size=count, replace=False)
                rows = rows[idx]
                cols = cols[idx]
            mask[rows, cols] = True    
            
        #Include "unclassified" and "dead"
        mask = np.logical_or(mask, self._LABELS==self._INT_MAP[utils.DEAD_NAME])
        mask = np.logical_or (mask, self._LABELS==self._INT_MAP[utils.UNCLASS_NAME])
        
        sample_labels = self._LABELS.copy()
        sample_labels[~mask] = self._INT_MAP[utils.UNCLASS_NAME]        
        
        return pipe_labels(categories=self._CATEGORIES,
                           colors=self._COLORS,
                           labels=sample_labels,
                           name=name)
    
    
    def remove(self, cats):
        """
        Remove a category or a list of categories
        
        Parameters:
            cats (str or list): the category or categories to remove
        """
        if type(cats) in [str, np.str_]:
            assert cats in self._CATEGORIES, "Cannot remove {}".format(cats)
            assert cats != utils.UNCLASS_NAME, "Can't remove 'Unclassified'"
            assert cats != utils.DEAD_NAME, "Can't remove 'Dead'"
            cats = [cats]
        elif isinstance(cats, Iterable):
            for cat in cats:
                assert cat in self._CATEGORIES, "Cannot remove {}".format(cat)
                assert cat != utils.UNCLASS_NAME, "Can't remove '{}'".format(utils.UNCLASS_NAME)
                assert cat != utils.DEAD_NAME, "Can't remove '{}'".format(utils.DEAD_NAME)
        
        mask = np.ones(len(self._CATEGORIES))
        for cat in cats:
            mask = np.logical_and(mask, self._CATEGORIES != cat)
        self._CATEGORIES = self._CATEGORIES[mask]
        self._COLORS = self._COLORS[mask]
        old_int_map = self._INT_MAP
        old_labels = self._LABELS
        self._construct_maps(self._CATEGORIES, self._COLORS)   
        self._LABELS = self._align_labels(old_int_map, old_labels)        
        
    def set_name(self, new_name):
        """
        Set the _NAME class attribute
        
        Parameters:
            new_name (str)
        """
        
        assert type(new_name) in [str, np.str_], "new_name must be of type string"
        self._NAME = new_name
    
    
    def show(self, save_path=None):
        """
        Displays the LABELS. Default behavior is to display to the terminal. If
        save_path is specified, the image is saved to save_path.
        
        Parameters:
            save_path (str) optional: path to save the image to
        """
        if save_path is None: #Output to terminal
            self._show()            
            plt.show()
        else: #Output to file
            assert type(save_path) in [str, np.str_], "save_path must be of type str"
            self._show()
            plt.savefig(fname=save_path)
        plt.close()
    
    
    def _show(self):
        """
        Helper function
        
        Called by show() and save_image()
        """      
        
        num_cats = len(self._CATEGORIES)        
        plt.figure(num=None, figsize=(9, 7), dpi=80, facecolor='w', edgecolor='k')
        cmap = matplotlib.colors.ListedColormap(self._COLORS)
        plt.imshow(self._LABELS, cmap=cmap, vmin=0, vmax=num_cats-1)
        ax = plt.colorbar()
        scale = (num_cats-1)/num_cats
        shift = (num_cats-1)/(2*num_cats)
        ax.set_ticks(np.arange(num_cats)*scale + shift)
        ax.set_ticklabels(self._CATEGORIES)
        plt.title("Cuprite Classified MSI")
        plt.tight_layout()
        plt.title(self._NAME)


    def __str__(self):
        """Called from print()"""
        return "NAME: {}\n\nCATEGORIES: {}\n\nINT_MAP: {}\n\nCOLOR_MAP: {}".format(self._NAME,
                                                                                   self._CATEGORIES,
                                                                                   self._INT_MAP,
                                                                                   self._COLOR_MAP)
        
    
    def train_test_split(self,
                         test_size=None,
                         train_size=None,
                         random_state=None,
                         shuffle=True):
        """
        A wrapper function for sklearn's train_test split
        
        Parameters:
            test_size (float, int, or None) optional: If float, should be 
                between 0.0 and 1.0 and represent the proportion of the dataset
                to include in the test split. If int, represents the absolute
                number of test samples. If None, the value is set to the
                complement of the train size. If train_size is also None, it
                will be set to 0.25.
            train_size (float, int, or None): If float, should be between 0.0
                and 1.0 and represent the proportion of the dataset to include
                in the train split. If int, represents the absolute number of
                train samples. If None, the value is automatically set to the
                complement of the test size.
            random_state (int, RandomState instance or None): f int,
                random_state is the seed used by the random number generator;
                If RandomState instance, random_state is the random number
                generator; If None, the random number generator is the
                RandomState instance used by np.random.
            shuffle (bool): Whether or not to shuffle the data before splitting.
                If shuffle=False then stratify must be None
                
        Returns:
            (pipe_labels): the training pipe_labels instance
            (pipe_labels): the testing pipe_labels instance
        """
        
        #FIXME: allow stratified splits so that original class distributions
        #are preserved in train and test
        
        
        live_mask = self.get_live_mask()
        locs = np.argwhere(live_mask)
        rows, cols = zip(*locs)
        
        train_rows, test_rows, train_cols, test_cols = train_test_split(rows,
                                                                        cols,
                                                                        test_size=test_size,
                                                                        train_size=train_size,
                                                                        random_state=random_state,
                                                                        shuffle=shuffle)
        
        #Construct train pipe_labels
        train_labels = np.ones((utils.NUM_ROWS, utils.NUM_COLS))*self._INT_MAP[utils.UNCLASS_NAME]
        #Add 'dead's if needed
        if utils.DEAD_NAME in self._CATEGORIES:
            dead_mask = self._LABELS == self._INT_MAP[utils.DEAD_NAME]
            train_labels[dead_mask] = self._INT_MAP[utils.DEAD_NAME]
        train_labels[train_rows, train_cols] = self._LABELS[train_rows, train_cols]
        train = pipe_labels(categories=self._CATEGORIES,
                            colors=self._COLORS,
                            labels=train_labels,
                            name="Train")
        
        #Construct test pipe_lbels
        test_labels = np.ones((utils.NUM_ROWS, utils.NUM_COLS))*self._INT_MAP[utils.UNCLASS_NAME]
        #Add 'dead's if needed
        if utils.DEAD_NAME in self._CATEGORIES:
            test_labels[dead_mask] = self._INT_MAP[utils.DEAD_NAME]
        test_labels[test_rows, test_cols] = self._LABELS[test_rows, test_cols]
        test = pipe_labels(categories=self._CATEGORIES,
                           colors=self._COLORS,
                           labels=test_labels,
                           name="Test")
        
        return train, test
              
    
    def union(self, other, name):
        """Allows two pipe_line instances to be combined into one.
        
        Colors from the original instance are given priority when there are
        conflicts
        
        Parameters:
            other (pipe_labels): a pipe_labels instance
            name (str): the name for the resulting pipe_labels instance
            
        Returns:
            (pipe_labels): the new pipe_labels instance
        """
        #Check input
        assert other is not self, "Cannot perform union on self"
        assert isinstance(other, pipe_labels), "other must be an instace of pipe_labels"
        
        #Update Categories
        new_categories = np.array(list(set(self._CATEGORIES).union(set(other._CATEGORIES))))
        new_categories.sort()
        #Put "unclassified" and "dead" last
        assert utils.UNCLASS_NAME in new_categories, "error in union()"
        unc_mask = new_categories == utils.UNCLASS_NAME
        new_categories = np.concatenate([new_categories[~unc_mask],
                                         new_categories[unc_mask]])
        if utils.DEAD_NAME in new_categories:
            dead_mask = new_categories == utils.DEAD_NAME
            new_categories = np.concatenate([new_categories[~dead_mask], 
                                             new_categories[dead_mask]])
        
        #Update Colors
        new_colors = []
        for cat in new_categories:
            if cat in self._CATEGORIES: #first instance gets priority
                new_colors.append(self._COLOR_MAP[cat])
            elif cat in other._CATEGORIES:
                new_colors.append(other._COLOR_MAP[cat])
            else:
                #Shouldn't get here
                assert False, "error in union()"
                
        assert len(new_categories) == len(new_colors), "new_categories and new_colors must have same length"
        new_INT_MAP = dict(zip(new_categories, np.arange(len(new_categories))))             
        
        
        #Update labels
        new_labels = np.ones((utils.NUM_ROWS, utils.NUM_COLS))*new_INT_MAP[utils.UNCLASS_NAME]
        
        for cat in new_categories:
            if cat == utils.UNCLASS_NAME:
                continue
            tar_int = new_INT_MAP[cat]
            #Get locations in original instance
            if cat in self._CATEGORIES:
                tar1 = self._INT_MAP[cat]
                mask1 = self._LABELS == tar1
            else:
                mask1 = np.zeros((utils.NUM_ROWS, utils.NUM_COLS))
            if cat in other._CATEGORIES:
                tar2 = other._INT_MAP[cat]
                mask2 = other._LABELS == tar2
            else:
                mask2 = np.zeros((utils.NUM_ROWS, utils.NUM_COLS))
                
            mask = np.logical_or(mask1, mask2)
            new_labels[mask] = tar_int        
        
        return pipe_labels(categories=new_categories,
                           colors=new_colors,
                           labels=new_labels,
                           name=name)