# -*- coding: utf-8 -*-

import numpy as np
from matplotlib import colors as mcolors

class pipe_settings():
    def __init__(self, categories, colors):
        #Check input
        assert len(categories) == len(colors), "categories and colors must have same length"
        
        #Check categories
        for cat in categories:
            assert type(cat) in [str, np.str_], "Each category must be of type string"    
            
        #Get named matplotlib colors
        cols = dict(mcolors.BASE_COLORS, **mcolors.CSS4_COLORS)
        by_hsv = sorted((tuple(mcolors.rgb_to_hsv(mcolors.to_rgba(color)[:3])), name)
                        for name, color in cols.items())
        named_colors = [name for hsv, name in by_hsv]
        
        #Check colors
        for col in colors:
            assert type(col) in [str, np.str_], "Each color must be of type string"
            assert col in named_colors, "Each color must be named in matplotlib; please see https://matplotlib.org/examples/color/named_colors.html"
            
        #Set class members
        self.CATEGORIES = np.array(categories)
        self.COLORS = np.array(colors)
        self.INT_MAP = dict(zip(categories, range(len(categories))))
        self.COLOR_MAP = dict(zip(categories, colors))
        
        
    def get_categories(self, labels):
        """
        FIXME: clean up this function
        Retrieves the categories (material names) given integer labels
        
        Parameters:
            labels (ndarray): integer labels
            
        Returns
            categories ((m, ) ndarray): the material names
        """
        #Check input
        #FIXME
        
        #Get sorted unique integers in labels
        unique = np.unique(labels).astype(int)
        assert np.all(unique >= 0), "labels must be nonnegative"
        
        #FIXME
#        if classified:
#            desired_int = np.array([settings.INT_MAP[i] for i in self.CLASS_CATEGORIES])
#            unique = unique[np.isin(unique, desired_int)]
               
        #Get categories from settings
        categories = np.array([self.CATEGORIES[i] for i in unique])
        return categories
    
    def __str__(self):
        """Called from print()"""
        return "CATEGORIES: {}\n\nINT_MAP: {}\n\nCOLOR_MAP: {}".format(self.CATEGORIES,
                                                                       self.INT_MAP,
                                                                       self.COLOR_MAP)