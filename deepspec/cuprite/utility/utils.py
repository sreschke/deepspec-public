# -*- coding: utf-8 -*-
"""
settings.py

Contributors:
    Spencer Reschke

Settings for the HSI project
"""
import numpy as np

###############################################################################
#CHANGE DATA_DIR FOR YOUR INSTALLATION
###############################################################################
DATA_DIR = 'C:\\Users\\spenc\\Home\\Work\\STL\\Projects\\HSI\\DeepSpec\\deepspec\\cuprite\\data\\'
###############################################################################

#These capture KRUSE's implicit integer mapping. E.g. Kruse implicitly defines
#'Unclassified' to be 0, 'Alunite' to be 1, and so on.
KR_CATEGORIES = np.array(['Unclassified', 'Alunite', 'Buddingtonite', 'Kaolinite', 'Calcite', 'Silica', 'Muscovite'])
KR_INT_MAP = dict(zip(KR_CATEGORIES, range(len(KR_CATEGORIES))))

#Other useful variables that are used throughout the project
NUM_ROWS = 1382
NUM_COLS = 1207
WEIGHT = 10000

UNCLASS_NAME = "Unclassified"
DEAD_NAME = "Dead"