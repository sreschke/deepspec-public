# -*- coding: utf-8 -*-
"""
misc.py

6/3/19
"""
import numpy as np
from sklearn.model_selection import train_test_split
from deepspec.cuprite.utility import utils

def get_classified(settings, spectra, labels):
    """
    Retrieves spectra and labels that are not "dead" or "unclassified"
    
    Parameters:
        settings (pipe_settings): a pipe_settings instantiation
        spectra ((m, n, o) ndarray): the spectral cuprite cube
        labels ((m, n) ndarray): the corresponding labels
        
    Returns:
        ((l, o) ndarray): the classified spectra
        ((l, ) ndarray): the classified labels
    """
    #Get classified spectra
    mask = np.logical_and(labels!=settings.INT_MAP["Dead"],
                          labels!=settings.INT_MAP["Unclassified"])
    class_spectra = spectra[mask]
    class_labels = labels[mask]
    
    return class_spectra, class_labels

def kruse_sample(settings, labels, per=None, count=None):
    """
    FIXME: write description
    If per is not None, sample will have same distribution as original
    If count is not None, sample will have uniform distribution
    
    Parameters:
        settings (pipe_settings): a pipe_settings instantiation
        labels ((m, n) ndarray): the spectral labels
        per (float): the percentage of data to use in the sample
        
    Returns:
        FIXME:       
    """
    #Check input
    assert type(labels) == np.ndarray, "labels must be of type ndarray"
    unique = np.unique(labels)
    if per is not None:        
        assert 0 < per < 1, "per must be between 0 and 1"
    if count is not None:
        #Make sure there are enough samples in each category
        counts = np.array([np.count_nonzero(labels == i) for i in unique])
        assert np.all(count < counts), "There are not {} samples from each category. Counts: {}".format(count, counts)
    assert per is not None or count is not None, "per and count cannot both be None"
        
    
    #Get categories
    categories = settings.get_categories(labels)
    mask = np.zeros((utils.NUM_ROWS, utils.NUM_COLS), dtype=bool)          
    #Sample from each category
    for cat in categories:
        tar_int = settings.INT_MAP[cat]
        locs = [list(j) for j in np.argwhere(labels==tar_int)]
        rows, cols = zip(*locs)
        rows = np.array(rows)
        cols = np.array(cols)
        if per is not None:
            rows, _, cols, _ = train_test_split(rows, cols, train_size=per)
        elif count is not None:
            idx = np.random.choice(a=np.arange(len(rows)), size=count, replace=False)
            rows = rows[idx]
            cols = cols[idx]
        mask[rows, cols] = True    
        
    #Include "unclassified" and "dead"
    mask = np.logical_or(mask, labels==settings.INT_MAP["Dead"])
    mask = np.logical_or (mask, labels==settings.INT_MAP["Unclassified"])
    
    sample_labels = labels.copy()
    sample_labels[~mask] = settings.INT_MAP["Unclassified"]
    return sample_labels
    
def section(string, char="=", num=70):
    """
    Prints string between two lines of char characters
    
    Parameters:
        string (str) the string to print
        char (str) optional: the charater to use in the lines. Default is "=".
        num (int) optional: the number of chars to use for each line. Default
            is 100
    """
    print(char*num)
    print(string)
    print(char*num)
    
def split_and_merge(X1, y1, X2, y2, per1, per2):
    """
    FIXME: does this belong in this file?
    
    Mixes data in the datasets (X1, y1) and (X2, y2) using per1 percent of
    dataset 1 and per2 percent of dataset 2. Data is randomly shuffled when
    merging the datasets.
    
    Parameters:
        X1 ((m, n) ndarray): 
        y1 ((m, ) ndarray): 
        X2 ((p, n) ndarray):
        y2 ((p, ) ndarray):
        train_per1 (float): percentage of (X1, y1) to use for train set. Must
            be in the range (0, 1] (including 1)
        train_per2 (float): percentage of (X2, y2) to use fro train set. Must 
            be in the range (0, 1) (excluding 1)
    
    Returns:
        X_train ((m, n) ndarray): the training input
        X_test ((p, n)) ndarray):the testing input
        y_train ((m, ) ndarray): the training labels
        y_test ((p, ) ndarray): the training labels
    """
    #Check Inputs
    assert type(X1) == np.ndarray, "X1 must be a numpy array"
    assert type(y1) == np.ndarray, "y1 must be a numpy array"
    assert type(X2) == np.ndarray, "X2 must be a numpy array"
    assert type(y2) == np.ndarray, "y2 must be a numpy array"
    assert X1.shape[0] == y1.shape[0], "X1 and y1 must have same shape along axis 0"
    assert X2.shape[0] == y2.shape[0], "X2 and y2 must have same shape along axis 0"
    assert X1.shape[1] == X2.shape[1], "X1 and X2 must have same shape along axis 1"
    assert per1 > 0 and per1 <= 1, "per1 must be in (0, 1]"
    assert per2 > 0 and per2 < 1, "per2 must be in (0, 1)"
    
    #Split X1 and y1
    if per1 < 1.0 and per1 > 0:
        X1_train, X1_test, y1_train, y1_test = train_test_split(X1, y1, train_size=per1)
    elif per1 == 1.0:
        X1_train = X1 
        y1_train = y1
    else:
        raise ValueError("per1 must be in (0, 1]")
    
    #Split X2 and y2
    if per2 < 1.0 and per2 > 0: #Redundant checks
        X2_train, X2_test, y2_train, y2_test = train_test_split(X2, y2, train_size=per2)
    else:
        raise ValueError("per2 must be in (0, 1)")
        
    #Merge Data
    X_train = np.vstack([X1_train, X2_train])
    y_train = np.concatenate([y1_train, y2_train])
    
    if per1 < 1.0 and per1 > 0: #Redundant checks
        X_test = np.vstack([X1_test, X2_test])
        y_test = np.concatenate([y1_test, y2_test])
    elif per1 == 1.0:
        X_test = X2_test
        y_test = y2_test
    else:
        raise ValueError("per1 must be in (0, 1]")
    
    #Shuffle Data along axis 0
    p1 = np.random.permutation(len(X_train))
    X_train = X_train[p1]
    y_train = y_train[p1]
    p2 = np.random.permutation(len(X_test))
    X_test = X_test[p2]
    y_test = y_test[p2]
    
    return X_train, X_test, y_train, y_test    