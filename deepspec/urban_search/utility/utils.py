# -*- coding: utf-8 -*-

# -*- coding: utf-8 -*-
"""
utils.py

Contributors:
    Spencer Reschke

9/14/2019
"""
import numpy as np

###############################################################################
#CHANGE DATA_DIR FOR YOUR INSTALLATION
###############################################################################
DATA_DIR = 'C:\\Users\\spenc\\Home\\Work\\STL\\Projects\\HSI\\DeepSpec\\deepspec\\urban_search\\data\\'
###############################################################################

#These capture the implicit mapping in the urban search data. 'HEU' is mapped to
#0, 'WGPu' is mapped to 1 and so on
URS_CATEGORIES = np.array(['HEU', 'WGPu', 'I131', 'Co60', 'Tc99m', 'TcHe', 'BG'])
URS_INT_MAP = dict(zip(URS_CATEGORIES, range(len(URS_CATEGORIES))))

#Other useful variables that are used throughout the project
NUM_ROWS = 64
NUM_COLS = 64