# -*- coding: utf-8 -*-
import os
import numpy as np
from deepspec.urban_search.utility import utils

def load_urb_search(file_name=None):
    """Loads the urban search gamma images and labels
    
    Parameters:
        file_name (str) optional: where to load the data. Assumes the data is
            located in the data file under urban_search. 
        
    Returns:
        X ((7029, 64, 64, 1) ndarray): the gamma images
        Y ((7029, ) ndarray): the integer labels
        class_names (list(str)): the material/class names        
    """
    
    if file_name is None:
        file_name = utils.DATA_DIR + os.sep + "gamma_data.npz"
        
    data = np.load(file_name)
    X = data["X"]
    Y = data["Y"]
    class_names = data["class_names"]
    
    return X, Y, class_names
    
    