# Deepspec

## Installation
NOTE: The public version of Deepspec does not currently include the data needed to run Deepspec. As such, Deepspec will NOT work and installation should not be attempted.

Please see [installation_instructions.md](https://bitbucket.org/sreschke/deepspec-public/src/master/installation_instructions.md)

## Overview
This repository contains the code base for the Hyperspectral Imaging (HSI) Site Directed Research Development (SDRD) at the Special Technologies Laboratory (STL) in Santa Barbara, California.

The SDRD seeks to leverage machine learning to perform automated spectral identification. That is, given a spectrum, we'd like to use a trained machine learning algorithm to classify the material (or mixture of materials) that produced the spectrum. Deepspec is intended to be used to build the machine learning pipelines STL will need to fullfill the goals of the HSI SDRD. Currently, Deepspec consists of a library of code needed to load, clean, and maniputate the Cuprite data used in the SDRD (see Cuprite Data section). Furthermore, it provides functions needed to evaluate the performance of machine learning algorithms trained on Cuprite spectra. Finally, Deepspec also provideds an organizational framework for future machine learning endeavors at STL.

Deepspec has been designed and implemented by Spencer Reschke who interned at STL during the summer of 2019. Future contributions from STL scientists and engineers are anticipated and welcome. 


## Cuprite Data
![Cuprite_Satelite](https://bitbucket.org/sreschke/deepspec-public/raw/4d4b592dfb8d65295722d3d3518e4be8f52f0fd2/deepspec/images/cuprite.PNG)

The available data for this project have come from satelite and airborn collects of Cuprite, Nevada, a site located roughly 200 miles northwest of Las Vegas. The datasets have been provided by Dr. Fred Kruse, who's performed much of the previous work on Cuprite spectra. The datasets include roughly 1.5 million spectra, half of which are labeled. Kruse's published results are shown below

![Kruse Cuprite Labels](https://bitbucket.org/sreschke/deepspec-public/raw/4d4b592dfb8d65295722d3d3518e4be8f52f0fd2/deepspec/images/kruse_ground_truth.png)

These predictions were obtained using a template matching algorithm called mixture tuned match filtering. The templates used in the algorithm were obtained from hand-picked regions of interest (ROI) in Cuprite, shown below

![Kruse_Cuprite_ROI](https://bitbucket.org/sreschke/deepspec-public/raw/4d4b592dfb8d65295722d3d3518e4be8f52f0fd2/deepspec/images/kruse_roi.png)

The materials dealt with in this project have so far been limited to Alunite, Buddingtonite, Calcite, Kaolinite, Muscovite, and Silica. Some representative samples are shown here
![Processed_HSI_spectra_samples](https://bitbucket.org/sreschke/deepspec-public/raw/4d4b592dfb8d65295722d3d3518e4be8f52f0fd2/deepspec/images/processed_spectra_samples.png)
## Demos
To get familiar with Deepspec, start off with these [demos](https://bitbucket.org/sreschke/deepspec-public/src/master/demos/).
## Current Accomplishments
* Established baseline accuracies with K-nearest neighbor and random forest algorithms
* Replicated Kruse's results using neural networks and less than 1% of the data]
## Future goals

Transfer Learning (Promising preliminary results)

n-shot learning via Siamese networks (Promising preliminary results)

embeddings rather than PCA

