NOTE: The public version of Deepspec does not currently include the data needed to run Deepspec. As such, Deepspec will NOT work and installation should not be attempted.

1) Download Deepspec repository

2) Run setup.py (file removed in public version of Deepspec)
    
3) Install dependencies* with pip
    
*  numpy (https://numpy.org/)
*  matplotlib (https://matplotlib.org/)
*  scikit-learn (https://scikit-learn.org/stable/)
*  spectral (http://www.spectralpython.net/installation.html)
*  imageio (https://pypi.org/project/imageio/)
*  tabulate (https://pypi.org/project/tabulate/)

*Note that some of the [demos](https://bitbucket.org/sreschke/deepspec-public/src/master/demos/) also rely on the gpu version of tensorflow or keras
