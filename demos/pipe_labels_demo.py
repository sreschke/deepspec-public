# -*- coding: utf-8 -*-
"""
pipe_labels.py

Demonstrates basic usage of pipe_labels class
"""

from deepspec.cuprite.data_processing.hsi import get_cuprite_hsi_cube,\
get_kruse_labels, hsi_preprocessing
from deepspec.cuprite.utility.misc import section

###############################################################################
#Load Data
###############################################################################
section("Load Data")
#Load ground truth pipe_labels class instantiation
gt = get_kruse_labels()

#Load unprocessed hsi spectra 
unp_spectra = get_cuprite_hsi_cube()

#print pipe_labels instance
print(gt)

#Visualize spectral image
gt.show()

#Visualize Class distributions
gt.bar_chart(count=True)

###############################################################################
#Remove a category
###############################################################################
section("Remove a category")
new_name = "pipe labels 1"
pl1 = gt.copy(new_name)
pl1.remove("Buddingtonite")
print(pl1)
pl1.show()
pl1.bar_chart(count=True)

###############################################################################
#Remove multiple categories
###############################################################################
section("Remove multiple categories")
new_name = "pipe labels 2"
pl2 = gt.copy(new_name)
pl2.remove(["Alunite", "Kaolinite"])
print(pl2)
pl2.show()
pl2.bar_chart(count=True)

###############################################################################
#Combine 2 pipe_labels instances
###############################################################################
section("Combine 2 pipe labels instances")
pl3 = pl1.union(pl2, "pipe labels 3")
print(pl3)
pl3.show()
pl3.bar_chart(count=True)

###############################################################################
#Create training set
###############################################################################
section("Create training set")
train = gt.random_sample(count=1000, name="Train")
print(train)
train.show()
train.bar_chart(count=True, live_only=True)

print("Loading spectra...")
unp_spectra = get_cuprite_hsi_cube()
print("Data preprocessing...")
pr_spectra, _ = hsi_preprocessing(unp_spectra, verbose=False)

print("Flattening data...")
fspectra, flabels, fsettings = train.flatten(pr_spectra)
print("Checking Shapes and settings...\n")
print("fspectra shape: {}".format(fspectra.shape))
print("flabels shape: {}".format(flabels.shape))
print("f_settings:")
print(fsettings)