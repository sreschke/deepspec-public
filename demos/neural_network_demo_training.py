# -*- coding: utf-8 -*-

import os
import numpy as np
import tensorflow as tf
from tensorflow.keras import layers
from sklearn.decomposition import PCA
from tensorflow.keras.callbacks import EarlyStopping, ModelCheckpoint
import pickle
from deepspec.cuprite.post_training.evaluation import plot_training_curves, plot_pr_curves, reliability_diagram

def run(settings,
        train_spectra,
        train_labels,
        val_spectra,
        val_labels,
        save_dir,
        model_name,
        verbose=True):
    """
    Parameters:
        settings (pipe_settings): a pipe_settings class instantiation
        train_spectra ((m, o) ndarray): training spectra
        train_labels ((m, ) ndarray): training labels (integers with zero indexing)
        val_spectra ((n, o) ndarray): validation spectra
        val_labels ((n, ) ndarray): validation labels (integers with zero indexing)
        save_dir (str): the directory to save output files to
        verbose (bool): whether to print function progress
    """
    #Check input
    assert type(train_spectra) == np.ndarray, "train_spectra must be of type ndarray"
    assert type(train_labels) == np.ndarray, "train_labels must be of type ndarray"
    assert type(val_spectra) == np.ndarray, "val_spectra must be of type ndarray"
    assert type(val_labels) == np.ndarray, "val_labels must be of type ndarray"
    assert train_spectra.shape[0] == train_labels.shape[0], "train_spectra and train_labels are not aligned"
    assert val_spectra.shape[0] == val_labels.shape[0], "val_spectra and val_labels are not aligned"
    assert type(save_dir) == str, "save_dir must be of type str"
    #Disable AVX2 warning
    tf.logging.set_verbosity(tf.logging.ERROR)
    
    #Make sure the GPU is being used
    os.environ["CUDA_VISIBLE_DEVICES"] = "0"
    assert "GPU:0" in tf.test.gpu_device_name(), "Tensorflow was not loaded with GPU"
    
    #Hyperparameters
    if verbose:
        print("Setting Model Hyperparameters...")
    layer_sizes = [60, 60] #sizes for hidden layers
    activations = ["relu", "relu"] #activations for hidden layers
    output_size = 6 #There are 6 material types
    learning_rate = 0.001
    optimizer = tf.keras.optimizers.Adam(learning_rate) #There are other hyperparameters for the Adam Optimizer
    #Also consider using RMSprop, Adagrad, and other optimizers
    batch_size = 32
    max_num_epochs = 100
    assert len(layer_sizes) == len(activations)
    
    #One hot_encode/binarize lables (e.g. [0, 1, 2] --> [[1, 0, 0], [0, 1, 0], [0, 0, 1]])
    oh_train_labels = tf.keras.utils.to_categorical(train_labels)
    assert len(oh_train_labels[0]) == output_size, "output_size and train_labels are not aligned"
    oh_val_labels = tf.keras.utils.to_categorical(val_labels)
    assert len(oh_val_labels[0]) == output_size, "output_size and val_labels are not aligned"
    
    #PCA
    if verbose:
        print("PCA...")
    num_components = 60
    pca = PCA(n_components=num_components)
    X_train = pca.fit_transform(train_spectra)
    y_train = oh_train_labels
    X_val = pca.transform(val_spectra)
    y_val = oh_val_labels
    
    #Save PCA model
    pca_save_name = model_name + "_PCA_transformation.sav"
    pca_save_path = os.path.join(save_dir, pca_save_name)
    pickle.dump(pca, open(pca_save_path, "wb"))
    
    #Construct Model
    if verbose:
        print("Constructing Model...")
    model = tf.keras.Sequential()
    
    #First hidden
    input_size = X_train.shape[1]
    model.add(layers.Dense(layer_sizes[0], activation=activations[0], input_shape=(input_size,)))
    
    #Hidden Layers
    for size, act in zip(layer_sizes[1:], activations[1:]):
        model.add(layers.Dense(size, activation=act))
        
    #Output Layer
    model.add(layers.Dense(output_size, activation="softmax"))
    
    #Compile Model
    model.compile(optimizer, 
                  loss="categorical_crossentropy", 
                  metrics=["accuracy"])
    
    #Training
    if verbose:
        print("Training... ")
    #Implementing Early stopping: see https://keras.rstudio.com/reference/callback_early_stopping.html
    es = EarlyStopping(monitor="val_acc",
                       patience=50,
                       mode="max",
                       verbose=1,
                       min_delta=0.01)
    
    
    model_save_path = os.path.join(save_dir, model_name + ".h5")
    mc = ModelCheckpoint(model_save_path, 
                         monitor="val_acc", 
                         mode='max', 
                         verbose=1, 
                         save_best_only=True)
    
    cb_list = [es, mc]
    history = model.fit(X_train, 
                        y_train,
                        validation_data = (X_val, y_val),
                        batch_size=batch_size,
                        callbacks = cb_list,
                        epochs = max_num_epochs)
    
    #Create Training Curves
    if verbose:
        print("Plotting Training Curves...")
    #Grab training histories
    train_acc = history.history["acc"]
    train_loss = history.history["loss"]
    val_acc = history.history["val_acc"]
    val_loss = history.history["val_loss"]
    epochs = np.arange(1, len(train_acc)+1)
    metrics = [train_acc, train_loss, val_acc, val_loss]
    colors = ["lime", "magenta", "green", "red"]
    labels = ["Train Accuracy", "Train Loss", "Validation Accuracy", "Validation Loss"]
    plot_training_curves(epochs, metrics, colors, labels, model_name)
    
    #Evaluate Model
    softmax_output = model.predict(X_val)
    
    #Make PR curves
    if verbose:
        print("Plotting PR curves...")
    plot_pr_curves(settings,
                   y_val, 
                   softmax_output)
    if verbose:
        print("Finished")
    
    #Make Reliability Diagram
    if verbose:
        print("Show reliability diagram...")
    reliability_diagram(softmax_output, val_labels)
    
    #Model summary
    print("Showing model summary...")
    print()
    print(model.summary())
    print()
    
    print("pca model saved to {}".format(pca_save_path))
    print("network saved to {}".format(model_save_path))    
            
    return model_save_path, pca_save_path