# Demos

These demos are intended to demonstrate the basic usage of Deepspec

* pipe_labels_demo.py shows basic usage of the the `pipe_labels` class
    * Demonstrates important `pipe_labels` methods including `bar_chart()`, `copy()`, `flatten()`, `random_sample()`, `remove()`, `show()`, and `union()`.
* knn_demo.py demonstrates how to build a machine learning pipeline using Deepspec and the K-nearest neighbors algorithm.
* random_forest_demo.py demonstates how to build a machine learning pipeling using Deepspec and the random forest algorithm
* nerual_network_demo.py demonstrates how to build a machine learning pipeline using Deepspec and neural networks.
    * Requires [tensorflow with gpu support](https://www.tensorflow.org/install/gpu)
    * neural_network_demo_training.py specifies the network training setup
    * Results saved in neural_network_demo_results folder