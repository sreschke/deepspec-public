# -*- coding: utf-8 -*-

from deepspec.cuprite.data_processing.hsi import get_cuprite_hsi_cube, get_kruse_labels, hsi_preprocessing
from deepspec.cuprite.data_visualization.visualize import visualize_predictions
from deepspec.cuprite.utility.misc import section
from deepspec.cuprite.post_training.inference import softmax_cube
import neural_network_demo_training
import numpy as np
import os

#Supress matplotlib warnings
import warnings
warnings.filterwarnings("ignore")

if __name__ == "__main__":
    model_name = "HSI_demo"
    save_dir = os.getcwd() + os.sep + "HSI_demo_results"
    
    ###########################################################################
    #Load Data
    ###########################################################################
    section("Load Data")
    #Load EVNI files
    print("Loading spectra...")
    unp_spectral_cube = get_cuprite_hsi_cube()
    print("Loading labels...")
    ground_truth = get_kruse_labels()
    
    #Visualize labels image
    print("Showing labels...")
    ground_truth.show()
    
    #Visualize class distribution
    print("Showing class distribution...")
    ground_truth.bar_chart(count=True)
    
    ###########################################################################
    #Data Preprocessing
    ###########################################################################
    section("Data Preprocessing")    
    #Visualize unprocessed samples
    print("Showing unprocessed spectra samples...")
    ground_truth.plot_spectra_samples(unp_spectral_cube, ave=True)    
    
    #Data Preprocessing
    pr_spectral_cube, pr_indices = hsi_preprocessing(spectral_cube=unp_spectral_cube)
    
    #Plot sample processed spectra
    print("Showing processing specta samples...")
    ground_truth.plot_processed_spectral_samples(pr_spectral_cube, indices=pr_indices,
                                                 ave=True)
    
    ###########################################################################
    #Training Set
    ###########################################################################
    section("Training Set")
    print("Creating training set...")
    train = ground_truth.random_sample(per=0.01, name="Train")
    print("Showing training set...")
    train.show()
    train.bar_chart(count=True, live_only=True)
    print("Flattening data...")
    ftrain_spectra, ftrain_labels, ftrain_settings = train.flatten(pr_spectral_cube)
    
    ###########################################################################
    #Validation Set
    ###########################################################################
    section("Validation Set")
    print("Creating validation set...")
    val = ground_truth.random_sample(per=0.01, name="Validation")    
    print("Showing validation set...")
    val.show()
    val.bar_chart(count=True, live_only=True)
    print("Flattening data...")
    fval_spectra, fval_labels, fval_settings = val.flatten(pr_spectral_cube)
    
    ###########################################################################
    #Network Training
    ###########################################################################
    section("Network Training")  
    model_path, pca_path = neural_network_demo_training.run(settings=ftrain_settings,
                                                            train_spectra=ftrain_spectra,
                                                            train_labels=ftrain_labels,
                                                            val_spectra=fval_spectra,
                                                            val_labels=fval_labels,
                                                            save_dir=save_dir,
                                                            model_name=model_name)
    
    ###########################################################################
    #Model Evaluation
    ###########################################################################    
    section("Model Evaluation")
    print("Inference with Thresholding...")
    gt_settings = ground_truth.get_settings()
    s_cube = softmax_cube(settings=gt_settings,
                          spectral_cube=pr_spectral_cube,
                          model_path=model_path,
                          pca_path=pca_path)
    
    #Visualize Predictions
    section("Prediction Visualization")    
    thresholds = np.linspace(0.2, 0.9, 8)  
    visualize_predictions(settings=gt_settings,
                          softmax_cube=s_cube,
                          save_dir=save_dir,
                          thresholds=thresholds)
    print("Prediction images saved to {}".format(save_dir))